package com.his2018.hismart.helper;

import android.app.Activity;
import android.util.DisplayMetrics;

/**
 * Created by TienMinh on 21-Jan-18.
 */

public class Helper {

    public static int getWidthScreen(Activity activity){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    public static int getHeightScreen(Activity activity){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

//    public static

}
