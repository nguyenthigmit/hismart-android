package com.his2018.hismart.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.his2018.hismart.R;
import com.his2018.hismart.helper.Helper;
import com.his2018.hismart.model.DoctorResponse;
import com.his2018.hismart.model.Patient;
import com.his2018.hismart.util.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PatientChartActivity extends AppCompatActivity {

    // toolbar
    @BindView(R.id.layoutToolbar)       LinearLayout layoutToolbar;
    @BindView(R.id.imgMainMenu)         ImageView imgMainMenu;
    @BindView(R.id.txtTitleToolbar)     TextView txtTitleToolbar;
    @BindView(R.id.imgPatientInfo)      ImageView imgPatientInfo;
    @BindView(R.id.imgPatientMessage)   ImageView imgPatientMessage;
    @BindView(R.id.imgPatientShare)     ImageView imgPatientShare;
    @BindView(R.id.layoutWarning)       RelativeLayout layoutWarning;
    @BindView(R.id.imgPatientWarning)   ImageView imgPatientWarning;
    @BindView(R.id.txtWarningCount)     TextView txtWarningCount;
    // tab bottom
    @BindView(R.id.layoutTabDoThi)      LinearLayout layoutTabDoThi;
    @BindView(R.id.imgTabDoThi)         ImageView imgTabDoThi;
    @BindView(R.id.txtTabDoThi)         TextView txtTabDoThi;
    @BindView(R.id.layoutTabKetQua)     LinearLayout layoutTabKetQua;
    @BindView(R.id.imgTabKetQua)        ImageView imgTabKetQua;
    @BindView(R.id.txtTabKetQua)        TextView txtTabKetQua;
    @BindView(R.id.layoutTabThongKe)    LinearLayout layoutTabThongKe;
    @BindView(R.id.imgTabThongKe)       ImageView imgTabThongKe;
    @BindView(R.id.txtTabThongKe)       TextView txtTabThongKe;
    @BindView(R.id.layoutTabChanDoan)  LinearLayout layoutTabChanDoan;
    @BindView(R.id.imgTabChanDoan)     ImageView imgTabChanDoan;
    @BindView(R.id.txtTabChanDoan)     TextView txtTabChanDoan;
    @BindView(R.id.layoutTabDonThuoc)   LinearLayout layoutTabDonThuoc;
    @BindView(R.id.imgTabDonThuoc)      ImageView imgTabDonThuoc;
    @BindView(R.id.txtTabDonThuoc)      TextView txtTabDonThuoc;
    // menu navi
    @BindView(R.id.drawer_layout)       DrawerLayout drawerLayout;
    @BindView(R.id.navigationView)      NavigationView navigationView;
    @BindView(R.id.layoutHeaderNavi)    LinearLayout layoutHeaderNavi;
    @BindView(R.id.txtHISmart)          TextView txtHISmart;
    @BindView(R.id.txtViewer)           TextView txtViewer;
    @BindView(R.id.layoutInfo)          RelativeLayout layoutInfo;
    @BindView(R.id.imgAccount)          ImageView imgAccount;
    @BindView(R.id.txtHoTenBacSi)       TextView txtHoTenBacSi;
    @BindView(R.id.txtMaSoBacSi)        TextView txtMaSoBacSi;
    @BindView(R.id.layoutMainMenu)              LinearLayout layoutMainMenu;
    @BindView(R.id.layoutThongTinTaiKhoan)      LinearLayout layoutThongTinTaiKhoan;
    @BindView(R.id.imgThongTinTaiKhoan)         ImageView imgThongTinTaiKhoan;
    @BindView(R.id.txtThongTinTaiKhoan)         TextView txtThongTinTaiKhoan;
    @BindView(R.id.layoutDanhSachBenhNhan)      LinearLayout layoutDanhSachBenhNhan;
    @BindView(R.id.imgDanhSachBenhNhan)         ImageView imgDanhSachBenhNhan;
    @BindView(R.id.txtDanhSachBenhNhan)         TextView txtDanhSachBenhNhan;
    @BindView(R.id.layoutHopThuTinNhan)         LinearLayout layoutHopThuTinNhan;
    @BindView(R.id.imgHopThuTinNhan)            ImageView imgHopThuTinNhan;
    @BindView(R.id.txtHopThuTinNhan)            TextView txtHopThuTinNhan;
    @BindView(R.id.layoutDanhSachDaChiaSe)      LinearLayout layoutDanhSachDaChiaSe;
    @BindView(R.id.imgDanhSachDaChiaSe)         ImageView imgDanhSachDaChiaSe;
    @BindView(R.id.txtDanhSachDaChiaSe)         TextView txtDanhSachDaChiaSe;
    @BindView(R.id.layoutDanhSachDuocChiaSe)    LinearLayout layoutDanhSachDuocChiaSe;
    @BindView(R.id.imgDanhSachDuocChiaSe)       ImageView imgDanhSachDuocChiaSe;
    @BindView(R.id.txtDanhSachDuocChiaSe)       TextView txtDanhSachDuocChiaSe;
    @BindView(R.id.layoutDangXuat)              LinearLayout layoutDangXuat;
    @BindView(R.id.imgDangXuat)                 ImageView imgDangXuat;
    @BindView(R.id.txtDangXuat)                 TextView txtDangXuat;

    int width, height;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_chart);

        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT < 23){
            layoutTabDoThi.setBackgroundColor(getResources().getColor(R.color.color_tab_selected));
        }else {
            layoutTabDoThi.setBackgroundColor(getColor(R.color.color_tab_selected));
        }

        Configuration configuration = getResources().getConfiguration();
        if (configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
            width   = Helper.getWidthScreen(this);
            height  = Helper.getHeightScreen(this);
            setLayoutViewVertical();

        }else {
            width   = Helper.getWidthScreen(this);
            height  = Helper.getHeightScreen(this);
            setLayoutViewHorizontal();
        }

        setInfoUser();
        addEvent();
    }

    private void setInfoUser() {
        if (Constant.IS_DOCTOR){
            DoctorResponse doctor = Constant.DOCTOR_LOGIN;
            if (doctor != null){
                txtHoTenBacSi.setText(doctor.getFullName());
                txtMaSoBacSi.setText(doctor.getUserID());
                txtTitleToolbar.setText(Constant.PATIENT_BY_DOCTOR.getPatient_Name());
            }
        }else {
            Patient patient = Constant.PATIENT_LOGIN;
            if (patient != null){
                txtHoTenBacSi.setText(patient.getPatient_Name());
                txtMaSoBacSi.setText(patient.getPID_ID());
                txtTitleToolbar.setText(patient.getPatient_Name());
            }
        }
    }

    private void addEvent() {

        /*
         * START ACTION IN TOOLBAR
         * */

        imgMainMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(Gravity.START);
            }
        });


        imgPatientInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), PatientInfomationActivity.class));
            }
        });

        imgPatientShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), SharePatientInfoActivity.class));
            }
        });

        imgPatientWarning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), BloodPressureAlarmActivity.class));
            }
        });

        /*
         * END ACTION IN TOOLBAR
         * */


        /*
         * START ACTION IN MENU NAVIGATIONVIEW CUSTOM
         * */
        layoutThongTinTaiKhoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), AccountInfomationActivity.class));
            }
        });

        layoutDanhSachBenhNhan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), PatientListActivity.class));
            }
        });

        layoutDanhSachDaChiaSe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), PatientInfoSharedListActivity.class));
            }
        });

        /*
         * END ACTION IN MENU NAVIGATIONVIEW CUSTOM
         * */

        layoutTabKetQua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PatientResultActivity.class));
                overridePendingTransition(0,0);
            }
        });

        layoutTabThongKe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PatientStatisticActivity.class));
                overridePendingTransition(0,0);
            }
        });

        layoutTabChanDoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PatientDiagnoseActivity.class));
                overridePendingTransition(0,0);
            }
        });

        layoutTabDonThuoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PatientPrescriptionActivity.class));
                overridePendingTransition(0,0);
            }
        });
    }

    private void setLayoutViewVertical() {
        setToolbarPatientVertical();
        setNavigationViewVertical();
        setTabBottomVertical();
    }

    private void setLayoutViewHorizontal() {
        setToolbarPatientHorizontal();
        setNavigationViewHorizontal();
        setTabBottomHorizontal();
    }

    private void setToolbarPatientVertical(){
        /*
        * TOOLBAR
        * */
        int paddingLR15 = width/30;
        int paddingTB5 = height/50;
        layoutToolbar.setPadding(paddingLR15, paddingTB5, paddingLR15, paddingTB5);

        int marginR15 = width/30;
        int height20 = height/30;
        LinearLayout.LayoutParams paramsMainMenu = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, height20);
        paramsMainMenu.setMargins(0, 0, marginR15, 0);
        imgMainMenu.setLayoutParams(paramsMainMenu);
        imgMainMenu.setAdjustViewBounds(true);

        float textSizeToolbar = height/40;
        txtTitleToolbar.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeToolbar);

        int marginL5 = width/50;
        int marginR10 = width/40;
        LinearLayout.LayoutParams paramsMenuAction = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, height20);
        paramsMenuAction.setMargins(marginL5, 0, marginR10, 0);
        imgPatientInfo.setLayoutParams(paramsMenuAction);
        imgPatientInfo.setAdjustViewBounds(true);

        imgPatientMessage.setLayoutParams(paramsMenuAction);
        imgPatientMessage.setAdjustViewBounds(true);

        imgPatientShare.setLayoutParams(paramsMenuAction);
        imgPatientShare.setAdjustViewBounds(true);

        int widthLayoutWarning = height/21;  // các icon menu action là 20dp, width layout warning là 25dp (chênh 5dp để gán cho khoảng cách cho noti count)
        LinearLayout.LayoutParams paramsLayoutWarning = new LinearLayout.LayoutParams(widthLayoutWarning, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsLayoutWarning.setMargins(marginL5, 0, 0, 0);
        layoutWarning.setLayoutParams(paramsLayoutWarning);

        int widthImgWarning = widthLayoutWarning*4/5;  // các icon menu action là 20dp, width layout warning là 25dp (chênh 5dp để gán cho khoảng cách cho noti count)
        RelativeLayout.LayoutParams paramsImgWarning = new RelativeLayout.LayoutParams(widthImgWarning, ViewGroup.LayoutParams.WRAP_CONTENT);
        imgPatientWarning.setLayoutParams(paramsImgWarning);
        imgPatientWarning.setAdjustViewBounds(true);

        int sizeWarningCount = widthLayoutWarning/2; // lấy size khoảng 12 của 25 layout warning
        float textSizeWarning = height/70;
        RelativeLayout.LayoutParams paramsWarningCount = new RelativeLayout.LayoutParams(sizeWarningCount, sizeWarningCount);
        paramsWarningCount.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        txtWarningCount.setLayoutParams(paramsWarningCount);
        txtWarningCount.setGravity(Gravity.CENTER);

        txtWarningCount.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeWarning);
    }

    private void setNavigationViewVertical(){
        /*
         * Set width NavigationView
         * */
        DrawerLayout.LayoutParams paramsNavi = new DrawerLayout.LayoutParams(width*4/5, ViewGroup.LayoutParams.MATCH_PARENT);
        paramsNavi.gravity = GravityCompat.START;
        navigationView.setLayoutParams(paramsNavi);

        /*
        * Header navi
        * */
        int paddingLR20 = width/25;
        layoutHeaderNavi.setPadding(paddingLR20, 0, paddingLR20, 0);

        float textSizeHIS = height/25;
        txtHISmart.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeHIS);

        float textSizeViewer = height/40;
        txtViewer.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeViewer);

        int paddingT10 = height/35;
        layoutInfo.setPadding(0, paddingT10, 0, 0);
        int marginB10 = height/35;
        int marginR20 = width/25;
        int height40 = height/14;
        int text_size = height/35;
        RelativeLayout.LayoutParams paramsAccount = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsAccount.setMargins(0, 0, marginR20, marginB10);
        imgAccount.setLayoutParams(paramsAccount);
        imgAccount.setAdjustViewBounds(true);
        imgAccount.setMaxHeight(height40);
        txtHoTenBacSi.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        txtMaSoBacSi.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);

        /*
        * MENU NAVI
        * */
        int padding20 = height/30;
        int paddingR40 = width/15;
        layoutMainMenu.setPadding(padding20, padding20, padding20, padding20);

        int height30 = height/20;
        int paddingTB5 = height/50;
        layoutThongTinTaiKhoan.setPadding(0, paddingTB5, 0, paddingTB5);
        LinearLayout.LayoutParams paramsMenuIcon = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, height30);
        imgThongTinTaiKhoan.setLayoutParams(paramsMenuIcon);
        imgThongTinTaiKhoan.setPadding(0, 0, paddingR40, 0);
        imgThongTinTaiKhoan.setAdjustViewBounds(true);
        txtThongTinTaiKhoan.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);

        layoutDanhSachBenhNhan.setPadding(0, paddingTB5, 0, paddingTB5);
        imgDanhSachBenhNhan.setLayoutParams(paramsMenuIcon);
        imgDanhSachBenhNhan.setPadding(0, 0, paddingR40, 0);
        imgDanhSachBenhNhan.setAdjustViewBounds(true);
        txtDanhSachBenhNhan.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);

        layoutHopThuTinNhan.setPadding(0, paddingTB5, 0, paddingTB5);
        imgHopThuTinNhan.setLayoutParams(paramsMenuIcon);
        imgHopThuTinNhan.setPadding(0, 0, paddingR40, 0);
        imgHopThuTinNhan.setAdjustViewBounds(true);
        txtHopThuTinNhan.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);

        layoutDanhSachDaChiaSe.setPadding(0, paddingTB5, 0, paddingTB5);
        imgDanhSachDaChiaSe.setLayoutParams(paramsMenuIcon);
        imgDanhSachDaChiaSe.setPadding(0, 0, paddingR40, 0);
        imgDanhSachDaChiaSe.setAdjustViewBounds(true);
        txtDanhSachDaChiaSe.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);

        layoutDanhSachDuocChiaSe.setPadding(0, paddingTB5, 0, paddingTB5);
        imgDanhSachDuocChiaSe.setLayoutParams(paramsMenuIcon);
        imgDanhSachDuocChiaSe.setPadding(0, 0, paddingR40, 0);
        imgDanhSachDuocChiaSe.setAdjustViewBounds(true);
        txtDanhSachDuocChiaSe.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);

        layoutDangXuat.setPadding(0, paddingTB5, 0, paddingTB5);
        imgDangXuat.setLayoutParams(paramsMenuIcon);
        imgDangXuat.setPadding(0, 0, paddingR40, 0);
        imgDangXuat.setAdjustViewBounds(true);
        txtDangXuat.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
    }

    private void setTabBottomVertical(){
        /*
        * TAB BOTTOM
        * */
        int paddingTBTab = height/80; // 5dp
        int height30 = height/30;
        float textSizeTab = height/45;
        layoutTabDoThi.setPadding(0, paddingTBTab, 0, paddingTBTab);
        LinearLayout.LayoutParams paramsImageTab = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height30);
        imgTabDoThi.setLayoutParams(paramsImageTab);
        imgTabDoThi.setAdjustViewBounds(true);
        txtTabDoThi.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);

        layoutTabKetQua.setPadding(0, paddingTBTab, 0, paddingTBTab);
        imgTabKetQua.setLayoutParams(paramsImageTab);
        imgTabKetQua.setAdjustViewBounds(true);
        txtTabKetQua.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);

        layoutTabThongKe.setPadding(0, paddingTBTab, 0, paddingTBTab);
        imgTabThongKe.setLayoutParams(paramsImageTab);
        imgTabThongKe.setAdjustViewBounds(true);
        txtTabThongKe.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);

        layoutTabChanDoan.setPadding(0, paddingTBTab, 0, paddingTBTab);
        imgTabChanDoan.setLayoutParams(paramsImageTab);
        imgTabChanDoan.setAdjustViewBounds(true);
        txtTabChanDoan.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);

        layoutTabDonThuoc.setPadding(0, paddingTBTab, 0, paddingTBTab);
        imgTabDonThuoc.setLayoutParams(paramsImageTab);
        imgTabDonThuoc.setAdjustViewBounds(true);
        txtTabDonThuoc.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);
    }

    private void setToolbarPatientHorizontal(){
        /*
        * TOOLBAR
        * */
        int paddingLR15 = width/70;
        int paddingTB5 = height/40;
        layoutToolbar.setPadding(paddingLR15, paddingTB5, paddingLR15, paddingTB5);

        int marginR15 = width/70;
        int height20 = height/20;
        LinearLayout.LayoutParams paramsMainMenu = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, height20);
        paramsMainMenu.setMargins(0, 0, marginR15, 0);
        imgMainMenu.setLayoutParams(paramsMainMenu);
        imgMainMenu.setAdjustViewBounds(true);

        float textSizeToolbar = height/25;
        txtTitleToolbar.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeToolbar);

        int marginL5 = width/90;
        int marginR10 = width/80;
        LinearLayout.LayoutParams paramsMenuAction = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, height20);
        paramsMenuAction.setMargins(marginL5, 0, marginR10, 0);
        imgPatientInfo.setLayoutParams(paramsMenuAction);
        imgPatientInfo.setAdjustViewBounds(true);

        imgPatientMessage.setLayoutParams(paramsMenuAction);
        imgPatientMessage.setAdjustViewBounds(true);

        imgPatientShare.setLayoutParams(paramsMenuAction);
        imgPatientShare.setAdjustViewBounds(true);

        int widthLayoutWarning = height/15;  // các icon menu action là 20dp, width layout warning là 25dp (chênh 5dp để gán cho khoảng cách cho noti count)
        LinearLayout.LayoutParams paramsLayoutWarning = new LinearLayout.LayoutParams(widthLayoutWarning, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsLayoutWarning.setMargins(marginL5, 0, 0, 0);
        layoutWarning.setLayoutParams(paramsLayoutWarning);

        int widthImgWarning = widthLayoutWarning*4/5;  // các icon menu action là 20dp, width layout warning là 25dp (chênh 5dp để gán cho khoảng cách cho noti count)
        RelativeLayout.LayoutParams paramsImgWarning = new RelativeLayout.LayoutParams(widthImgWarning, ViewGroup.LayoutParams.WRAP_CONTENT);
        imgPatientWarning.setLayoutParams(paramsImgWarning);
        imgPatientWarning.setAdjustViewBounds(true);

        int sizeWarningCount = widthLayoutWarning/2; // lấy size khoảng 12 của 25 layout warning
        float textSizeWarning = height/40;
        RelativeLayout.LayoutParams paramsWarningCount = new RelativeLayout.LayoutParams(sizeWarningCount, sizeWarningCount);
        paramsWarningCount.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        txtWarningCount.setLayoutParams(paramsWarningCount);
        txtWarningCount.setGravity(Gravity.CENTER);

        txtWarningCount.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeWarning);
    }

    private void setNavigationViewHorizontal(){
        /*
         * Set width NavigationView
         * */
        DrawerLayout.LayoutParams paramsNavi = new DrawerLayout.LayoutParams(width*3/5, ViewGroup.LayoutParams.MATCH_PARENT);
        paramsNavi.gravity = GravityCompat.START;
        navigationView.setLayoutParams(paramsNavi);

        /*
        * Header navi
        * */
        int paddingLR20 = width/35;
        layoutHeaderNavi.setPadding(paddingLR20, 0, paddingLR20, 0);
        float textSizeHIS = height/15;
        txtHISmart.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeHIS);
        float textSizeViewer = height/30;
        txtViewer.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeViewer);
        int paddingT10 = height/35;
        layoutInfo.setPadding(0, paddingT10, 0, 0);
        int marginB10 = height/25;
        int marginR20 = width/35;
        int text_size_navi = height/20;
        int height40 = height/8;
        RelativeLayout.LayoutParams paramsAccount = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsAccount.setMargins(0, 0, marginR20, marginB10);
        imgAccount.setLayoutParams(paramsAccount);
        imgAccount.setAdjustViewBounds(true);
        imgAccount.setMaxHeight(height40);
        txtHoTenBacSi.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_navi);
        txtMaSoBacSi.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_navi);

        /*
        * MENU NAVI
        * */
        int padding20 = height/20;
        int paddingR40 = width/25;
        layoutMainMenu.setPadding(padding20, padding20, padding20, padding20);

        int height30 = height/15;
        int paddingTB5 = height/40;
        layoutThongTinTaiKhoan.setPadding(0, paddingTB5, 0, paddingTB5);
        LinearLayout.LayoutParams paramsMenuIcon = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, height30);
        imgThongTinTaiKhoan.setLayoutParams(paramsMenuIcon);
        imgThongTinTaiKhoan.setPadding(0, 0, paddingR40, 0);
        imgThongTinTaiKhoan.setAdjustViewBounds(true);
        txtThongTinTaiKhoan.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_navi);

        layoutDanhSachBenhNhan.setPadding(0, paddingTB5, 0, paddingTB5);
        imgDanhSachBenhNhan.setLayoutParams(paramsMenuIcon);
        imgDanhSachBenhNhan.setPadding(0, 0, paddingR40, 0);
        imgDanhSachBenhNhan.setAdjustViewBounds(true);
        txtDanhSachBenhNhan.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_navi);

        layoutHopThuTinNhan.setPadding(0, paddingTB5, 0, paddingTB5);
        imgHopThuTinNhan.setLayoutParams(paramsMenuIcon);
        imgHopThuTinNhan.setPadding(0, 0, paddingR40, 0);
        imgHopThuTinNhan.setAdjustViewBounds(true);
        txtHopThuTinNhan.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_navi);

        layoutDanhSachDaChiaSe.setPadding(0, paddingTB5, 0, paddingTB5);
        imgDanhSachDaChiaSe.setLayoutParams(paramsMenuIcon);
        imgDanhSachDaChiaSe.setPadding(0, 0, paddingR40, 0);
        imgDanhSachDaChiaSe.setAdjustViewBounds(true);
        txtDanhSachDaChiaSe.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_navi);

        layoutDanhSachDuocChiaSe.setPadding(0, paddingTB5, 0, paddingTB5);
        imgDanhSachDuocChiaSe.setLayoutParams(paramsMenuIcon);
        imgDanhSachDuocChiaSe.setPadding(0, 0, paddingR40, 0);
        imgDanhSachDuocChiaSe.setAdjustViewBounds(true);
        txtDanhSachDuocChiaSe.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_navi);

        layoutDangXuat.setPadding(0, paddingTB5, 0, paddingTB5);
        imgDangXuat.setLayoutParams(paramsMenuIcon);
        imgDangXuat.setPadding(0, 0, paddingR40, 0);
        imgDangXuat.setAdjustViewBounds(true);
        txtDangXuat.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_navi);
    }

    private void setTabBottomHorizontal(){
        /*
        * TAB BOTTOM
        * */
        int paddingTBTab = height/50; // 5dp
        int height30 = height/20;
        float textSizeTab = height/30;
        layoutTabDoThi.setPadding(0, paddingTBTab, 0, paddingTBTab);
        LinearLayout.LayoutParams paramsImageTab = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height30);
        imgTabDoThi.setLayoutParams(paramsImageTab);
        imgTabDoThi.setAdjustViewBounds(true);
        txtTabDoThi.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);

        layoutTabKetQua.setPadding(0, paddingTBTab, 0, paddingTBTab);
        imgTabKetQua.setLayoutParams(paramsImageTab);
        imgTabKetQua.setAdjustViewBounds(true);
        txtTabKetQua.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);

        layoutTabThongKe.setPadding(0, paddingTBTab, 0, paddingTBTab);
        imgTabThongKe.setLayoutParams(paramsImageTab);
        imgTabThongKe.setAdjustViewBounds(true);
        txtTabThongKe.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);

        layoutTabChanDoan.setPadding(0, paddingTBTab, 0, paddingTBTab);
        imgTabChanDoan.setLayoutParams(paramsImageTab);
        imgTabChanDoan.setAdjustViewBounds(true);
        txtTabChanDoan.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);

        layoutTabDonThuoc.setPadding(0, paddingTBTab, 0, paddingTBTab);
        imgTabDonThuoc.setLayoutParams(paramsImageTab);
        imgTabDonThuoc.setAdjustViewBounds(true);
        txtTabDonThuoc.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}
