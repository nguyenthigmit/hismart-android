package com.his2018.hismart.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.his2018.hismart.R;
import com.his2018.hismart.helper.Helper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AnalysisStatisticalActivity extends AppCompatActivity {

    // toolbar
    @BindView(R.id.toolbar)             Toolbar toolbar;
    @BindView(R.id.txtTitleToolbar)     TextView txtTitleToolbar;

    // tab bottom
    @BindView(R.id.layoutTabDoThi)      LinearLayout layoutTabDoThi;
    @BindView(R.id.imgTabDoThi)         ImageView imgTabDoThi;
    @BindView(R.id.txtTabDoThi)         TextView txtTabDoThi;
    @BindView(R.id.layoutTabKetQua)     LinearLayout layoutTabKetQua;
    @BindView(R.id.imgTabKetQua)        ImageView imgTabKetQua;
    @BindView(R.id.txtTabKetQua)        TextView txtTabKetQua;
    @BindView(R.id.layoutTabThongKe)    LinearLayout layoutTabThongKe;
    @BindView(R.id.imgTabThongKe)       ImageView imgTabThongKe;
    @BindView(R.id.txtTabThongKe)       TextView txtTabThongKe;
    @BindView(R.id.layoutTabChanDoan)   LinearLayout layoutTabChanDoan;
    @BindView(R.id.imgTabChanDoan)      ImageView imgTabChanDoan;
    @BindView(R.id.txtTabChanDoan)      TextView txtTabChanDoan;
    @BindView(R.id.layoutTabDonThuoc)   LinearLayout layoutTabDonThuoc;
    @BindView(R.id.imgTabDonThuoc)      ImageView imgTabDonThuoc;
    @BindView(R.id.txtTabDonThuoc)      TextView txtTabDonThuoc;

    int width, height;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analysis_statistical);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        txtTitleToolbar.setText(R.string.phan_tich_thong_ke);


        if (Build.VERSION.SDK_INT < 23){
            layoutTabKetQua.setBackgroundColor(getResources().getColor(R.color.color_tab_selected));
        }else {
            layoutTabKetQua.setBackgroundColor(getColor(R.color.color_tab_selected));
        }

        Configuration configuration = getResources().getConfiguration();
        if (configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
            width   = Helper.getWidthScreen(this);
            height  = Helper.getHeightScreen(this);
            setLayoutViewVertical();

        }else {
            width   = Helper.getWidthScreen(this);
            height  = Helper.getHeightScreen(this);
            setLayoutViewHorizontal();
        }


        addEvent();
    }

    private void addEvent() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        layoutTabDoThi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PatientChartActivity.class));
                overridePendingTransition(0,0);
            }
        });

        layoutTabDonThuoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PatientPrescriptionActivity.class));
                overridePendingTransition(0,0);
            }
        });

        layoutTabThongKe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PatientStatisticActivity.class));
                overridePendingTransition(0,0);
            }
        });

        layoutTabChanDoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PatientDiagnoseActivity.class));
                overridePendingTransition(0,0);
            }
        });
    }

    private void setLayoutViewVertical() {
        setToolbarPatientVertical();
        setTabBottomVertical();
    }

    private void setLayoutViewHorizontal() {
        setToolbarPatientHorizontal();
        setTabBottomHorizontal();
    }

    private void setToolbarPatientVertical(){
        float textSizeToolbar = height/30;
        txtTitleToolbar.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeToolbar);
    }

    private void setTabBottomVertical(){
        /*
         * TAB BOTTOM
         * */
        int paddingTBTab = height/80; // 5dp
        int height30 = height/30;
        float textSizeTab = height/45;
        layoutTabDoThi.setPadding(0, paddingTBTab, 0, paddingTBTab);
        LinearLayout.LayoutParams paramsImageTab = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height30);
        imgTabDoThi.setLayoutParams(paramsImageTab);
        imgTabDoThi.setAdjustViewBounds(true);
        txtTabDoThi.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);

        layoutTabKetQua.setPadding(0, paddingTBTab, 0, paddingTBTab);
        imgTabKetQua.setLayoutParams(paramsImageTab);
        imgTabKetQua.setAdjustViewBounds(true);
        txtTabKetQua.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);

        layoutTabThongKe.setPadding(0, paddingTBTab, 0, paddingTBTab);
        imgTabThongKe.setLayoutParams(paramsImageTab);
        imgTabThongKe.setAdjustViewBounds(true);
        txtTabThongKe.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);

        layoutTabChanDoan.setPadding(0, paddingTBTab, 0, paddingTBTab);
        imgTabChanDoan.setLayoutParams(paramsImageTab);
        imgTabChanDoan.setAdjustViewBounds(true);
        txtTabChanDoan.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);

        layoutTabDonThuoc.setPadding(0, paddingTBTab, 0, paddingTBTab);
        imgTabDonThuoc.setLayoutParams(paramsImageTab);
        imgTabDonThuoc.setAdjustViewBounds(true);
        txtTabDonThuoc.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);
    }

    private void setToolbarPatientHorizontal(){
        float textSizeToolbar = height/20;
        txtTitleToolbar.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeToolbar);
    }

    private void setTabBottomHorizontal(){
        /*
         * TAB BOTTOM
         * */
        int paddingTBTab = height/50; // 5dp
        int height30 = height/20;
        float textSizeTab = height/30;
        layoutTabDoThi.setPadding(0, paddingTBTab, 0, paddingTBTab);
        LinearLayout.LayoutParams paramsImageTab = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height30);
        imgTabDoThi.setLayoutParams(paramsImageTab);
        imgTabDoThi.setAdjustViewBounds(true);
        txtTabDoThi.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);

        layoutTabKetQua.setPadding(0, paddingTBTab, 0, paddingTBTab);
        imgTabKetQua.setLayoutParams(paramsImageTab);
        imgTabKetQua.setAdjustViewBounds(true);
        txtTabKetQua.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);

        layoutTabThongKe.setPadding(0, paddingTBTab, 0, paddingTBTab);
        imgTabThongKe.setLayoutParams(paramsImageTab);
        imgTabThongKe.setAdjustViewBounds(true);
        txtTabThongKe.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);

        layoutTabChanDoan.setPadding(0, paddingTBTab, 0, paddingTBTab);
        imgTabChanDoan.setLayoutParams(paramsImageTab);
        imgTabChanDoan.setAdjustViewBounds(true);
        txtTabChanDoan.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);

        layoutTabDonThuoc.setPadding(0, paddingTBTab, 0, paddingTBTab);
        imgTabDonThuoc.setLayoutParams(paramsImageTab);
        imgTabDonThuoc.setAdjustViewBounds(true);
        txtTabDonThuoc.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);
    }
}
