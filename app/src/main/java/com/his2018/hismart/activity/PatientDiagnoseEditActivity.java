package com.his2018.hismart.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.his2018.hismart.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PatientDiagnoseEditActivity extends AppCompatActivity {

    // tab bottom
    @BindView(R.id.layoutTabDoThi)      LinearLayout layoutTabDoThi;
    @BindView(R.id.imgTabDoThi)         ImageView imgTabDoThi;
    @BindView(R.id.txtTabDoThi)         TextView txtTabDoThi;
    @BindView(R.id.layoutTabKetQua)     LinearLayout layoutTabKetQua;
    @BindView(R.id.imgTabKetQua)        ImageView imgTabKetQua;
    @BindView(R.id.txtTabKetQua)        TextView txtTabKetQua;
    @BindView(R.id.layoutTabThongKe)    LinearLayout layoutTabThongKe;
    @BindView(R.id.imgTabThongKe)       ImageView imgTabThongKe;
    @BindView(R.id.txtTabThongKe)       TextView txtTabThongKe;
    @BindView(R.id.layoutTabChanDoan)   LinearLayout layoutTabChanDoan;
    @BindView(R.id.imgTabChanDoan)      ImageView imgTabChanDoan;
    @BindView(R.id.txtTabChanDoan)      TextView txtTabChanDoan;
    @BindView(R.id.layoutTabDonThuoc)   LinearLayout layoutTabDonThuoc;
    @BindView(R.id.imgTabDonThuoc)      ImageView imgTabDonThuoc;
    @BindView(R.id.txtTabDonThuoc)      TextView txtTabDonThuoc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_diagnose_edit);

        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT < 23){
            layoutTabChanDoan.setBackgroundColor(getResources().getColor(R.color.color_tab_selected));
        }else {
            layoutTabChanDoan.setBackgroundColor(getColor(R.color.color_tab_selected));
        }

        addEvent();
    }

    private void addEvent() {

        layoutTabDoThi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PatientChartActivity.class));
                overridePendingTransition(0,0);
            }
        });

        layoutTabKetQua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PatientResultActivity.class));
                overridePendingTransition(0,0);
            }
        });

        layoutTabThongKe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PatientStatisticActivity.class));
                overridePendingTransition(0,0);
            }
        });

        layoutTabDonThuoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PatientPrescriptionActivity.class));
                overridePendingTransition(0,0);
            }
        });

    }
}
