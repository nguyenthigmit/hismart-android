package com.his2018.hismart.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.his2018.hismart.R;
import com.his2018.hismart.helper.Helper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterUserPasswordActivity extends AppCompatActivity {


    @BindView(R.id.layoutToolbar)           RelativeLayout layoutToolbar;
    @BindView(R.id.imgBackButton)           ImageView imgBackButton;
    @BindView(R.id.txtTitleToolbar)         TextView txtTitleToolbar;
    @BindView(R.id.txtHISmart)              TextView txtHISmart;
    @BindView(R.id.txtViewer)               TextView txtViewer;
    @BindView(R.id.container_password)      LinearLayout container_password;
    @BindView(R.id.txtMatKhau)              TextView txtMatKhau;
    @BindView(R.id.edtMatKhau)              EditText edtMatKhau;
    @BindView(R.id.txtNhapLaiMatKhau)       TextView txtNhapLaiMatKhau;
    @BindView(R.id.edtNhapLaiMatKhau)       EditText edtNhapLaiMatKhau;
    @BindView(R.id.btnHoanThanh)            Button btnHoanThanh;

    int width, height;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_register_user_password);

        ButterKnife.bind(this);

        Configuration configuration = getResources().getConfiguration();
        if (configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
            width   = Helper.getWidthScreen(this);
            height  = Helper.getHeightScreen(this);
            setLayoutViewVertical();

        }else {
            width   = Helper.getWidthScreen(this);
            height  = Helper.getHeightScreen(this);
            setLayoutViewHorizontal();
        }

        addEvents();
    }

    private void setLayoutViewHorizontal() {
        int padding10 = height/35;
        layoutToolbar.setPadding(padding10, padding10, padding10, padding10);
        int maxHeight30 = height/20;
        int paddingR15 = width/40;
        imgBackButton.setMaxHeight(maxHeight30);
        imgBackButton.setPadding(0, 0, paddingR15, 0);
        float text_size_dang_ki = height/25;
        txtTitleToolbar.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_dang_ki);
        float text_size_HISmart = height/25;
        txtHISmart.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_HISmart);
        float text_size_VIEWER = height/35;
        txtViewer.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_VIEWER);

        int paddingLR15 = width/40;
        int paddingTop50 = height/7;
        container_password.setPadding(paddingLR15, paddingTop50, paddingLR15, 0);
        float text_size = height/25;
        int paddingTop20 = height/25;
        txtMatKhau.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        edtMatKhau.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        txtNhapLaiMatKhau.setPadding(0, paddingTop20, 0, 0);
        txtNhapLaiMatKhau.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        edtNhapLaiMatKhau.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);

        // set corner radius button
        GradientDrawable shape =  new GradientDrawable();
        shape.setCornerRadius(height/10);
        if(Build.VERSION.SDK_INT >= 23){
            shape.setColor(getResources().getColor(R.color.color_gray, null));
        }else {
            shape.setColor(getResources().getColor(R.color.color_gray));
        }
        btnHoanThanh.setBackground(shape);

        int marginTop50 = height/8;
        int paddingButton = height/30;
        int widthButton = width*2/3;
        LinearLayout.LayoutParams paramsBtnHoanThanh = new LinearLayout.LayoutParams(widthButton, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsBtnHoanThanh.setMargins(0, marginTop50, 0, 0);
        paramsBtnHoanThanh.gravity = Gravity.CENTER_HORIZONTAL;
        btnHoanThanh.setLayoutParams(paramsBtnHoanThanh);
        btnHoanThanh.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        btnHoanThanh.setPadding(0, paddingButton, 0, paddingButton);
    }

    private void setLayoutViewVertical() {
        int padding10 = height/45;
        layoutToolbar.setPadding(padding10, padding10, padding10, padding10);
        int maxHeight30 = height/30;
        int paddingR15 = width/20;
        imgBackButton.setMaxHeight(maxHeight30);
        imgBackButton.setPadding(0, 0, paddingR15, 0);
        float text_size_dang_ki = height/25;
        txtTitleToolbar.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_dang_ki);
        float text_size_HISmart = height/25;
        txtHISmart.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_HISmart);
        float text_size_VIEWER = height/45;
        txtViewer.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_VIEWER);

        int paddingLR15 = width/20;
        int paddingTop50 = height/12;
        container_password.setPadding(paddingLR15, paddingTop50, paddingLR15, 0);
        float text_size = height/35;
        int paddingTop20 = height/35;
        txtMatKhau.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        edtMatKhau.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        txtNhapLaiMatKhau.setPadding(0, paddingTop20, 0, 0);
        txtNhapLaiMatKhau.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        edtNhapLaiMatKhau.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);

        // set corner radius button
        GradientDrawable shape =  new GradientDrawable();
        shape.setCornerRadius(height/20);
        if(Build.VERSION.SDK_INT >= 23){
            shape.setColor(getResources().getColor(R.color.color_gray, null));
        }else {
            shape.setColor(getResources().getColor(R.color.color_gray));
        }
        btnHoanThanh.setBackground(shape);

        int marginTop50 = height/12;
        int paddingButton = height/50;
        int widthButton = width*2/3;
        LinearLayout.LayoutParams paramsBtnHoanThanh = new LinearLayout.LayoutParams(widthButton, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsBtnHoanThanh.setMargins(0, marginTop50, 0, 0);
        paramsBtnHoanThanh.gravity = Gravity.CENTER_HORIZONTAL;
        btnHoanThanh.setLayoutParams(paramsBtnHoanThanh);
        btnHoanThanh.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        btnHoanThanh.setPadding(0, paddingButton, 0, paddingButton);
    }

    private void addEvents() {
        btnHoanThanh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), RegisterUserCompleteActivity.class));
            }
        });

        edtMatKhau.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                checkInput();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        edtNhapLaiMatKhau.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                checkInput();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void checkInput(){
        String matKhau          = edtMatKhau.getText().toString().trim();
        String matKhauNhapLai   = edtNhapLaiMatKhau.getText().toString().trim();

        int colorGray, colorPink;
        if (Build.VERSION.SDK_INT >= 23){
            colorGray   = getResources().getColor(R.color.color_gray, null);
            colorPink   = getResources().getColor(R.color.color_pink_light, null);
        }else {
            colorGray   = getResources().getColor(R.color.color_gray);
            colorPink   = getResources().getColor(R.color.color_pink_light);
        }

        // set corner radius button
        GradientDrawable shape =  new GradientDrawable();
        shape.setCornerRadius(height/20);
        if (matKhau.length() == 0 || matKhauNhapLai.length() == 0 ){
            btnHoanThanh.setEnabled(false);
            shape.setColor(colorGray);
        }else {
            btnHoanThanh.setEnabled(true);
            shape.setColor(colorPink);
        }
        btnHoanThanh.setBackground(shape);
    }
}
