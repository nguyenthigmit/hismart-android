package com.his2018.hismart.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.his2018.hismart.R;
import com.his2018.hismart.helper.Helper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LaunchScreenActivity extends AppCompatActivity {

    @BindView(R.id.txtHISmart)    TextView txtHISmart;
    @BindView(R.id.txtViewer)     TextView txtViewer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_launch_screen);

        ButterKnife.bind(this);

        setLayoutView();

        new CountDownTimer(1000, 500) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                finish();
            }
        }.start();
    }

    private void setLayoutView() {
        int height = Helper.getHeightScreen(this);
        txtHISmart.setTextSize(TypedValue.COMPLEX_UNIT_PX, height/20);
        txtViewer.setTextSize(TypedValue.COMPLEX_UNIT_PX, height/40);
    }
}
