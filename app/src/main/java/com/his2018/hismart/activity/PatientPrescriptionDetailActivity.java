package com.his2018.hismart.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.Toolbar;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.his2018.hismart.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PatientPrescriptionDetailActivity extends AppCompatActivity {

    // toolbar
    @BindView(R.id.toolbar)             Toolbar toolbar;
    @BindView(R.id.txtTitleToolbar)     TextView txtTitleToolbar;

    // tab bottom
    @BindView(R.id.layoutTabDoThi)      LinearLayout layoutTabDoThi;
    @BindView(R.id.imgTabDoThi)         ImageView imgTabDoThi;
    @BindView(R.id.txtTabDoThi)         TextView txtTabDoThi;
    @BindView(R.id.layoutTabKetQua)     LinearLayout layoutTabKetQua;
    @BindView(R.id.imgTabKetQua)        ImageView imgTabKetQua;
    @BindView(R.id.txtTabKetQua)        TextView txtTabKetQua;
    @BindView(R.id.layoutTabThongKe)    LinearLayout layoutTabThongKe;
    @BindView(R.id.imgTabThongKe)       ImageView imgTabThongKe;
    @BindView(R.id.txtTabThongKe)       TextView txtTabThongKe;
    @BindView(R.id.layoutTabChanDoan)   LinearLayout layoutTabChanDoan;
    @BindView(R.id.imgTabChanDoan)      ImageView imgTabChanDoan;
    @BindView(R.id.txtTabChanDoan)      TextView txtTabChanDoan;
    @BindView(R.id.layoutTabDonThuoc)   LinearLayout layoutTabDonThuoc;
    @BindView(R.id.imgTabDonThuoc)      ImageView imgTabDonThuoc;
    @BindView(R.id.txtTabDonThuoc)      TextView txtTabDonThuoc;

    @BindView(R.id.imgMenuPrescription)         ImageView imgMenuPrescription;

    int width, height;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_prescription_detail);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        txtTitleToolbar.setText(R.string.don_thuoc);


        if (Build.VERSION.SDK_INT < 23){
            layoutTabDonThuoc.setBackgroundColor(getResources().getColor(R.color.color_tab_selected));
        }else {
            layoutTabDonThuoc.setBackgroundColor(getColor(R.color.color_tab_selected));
        }


        addEvent();
    }

    private void addEvent() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        imgMenuPrescription.setOnClickListener(new View.OnClickListener() {

            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View view) {
                MenuBuilder menuBuilder = new MenuBuilder(PatientPrescriptionDetailActivity.this);
                MenuInflater inflater = new MenuInflater(PatientPrescriptionDetailActivity.this);
                inflater.inflate(R.menu.menu_prescription_detail, menuBuilder);
                MenuPopupHelper optionsMenu = new MenuPopupHelper(PatientPrescriptionDetailActivity.this, menuBuilder, view);
                optionsMenu.setForceShowIcon(true);
                optionsMenu.show();

                menuBuilder.setCallback(new MenuBuilder.Callback() {
                    @Override
                    public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {

                        switch (item.getItemId()){
                            case R.id.menuXemDoThiHuyetAp:
                                break;
                            case R.id.menuXemLichSu:
                                break;
                            case R.id.menuChinhSua:
                                startActivity(new Intent(getApplicationContext(), PatientPrescriptionEditActivity.class));
                                break;
                            case R.id.menuXoaDonThuoc:
                                break;
                        }

                        return false;
                    }

                    @Override
                    public void onMenuModeChange(MenuBuilder menu) {

                    }
                });
            }
        });

        layoutTabDoThi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PatientChartActivity.class));
                overridePendingTransition(0,0);
            }
        });

        layoutTabDonThuoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PatientPrescriptionActivity.class));
                overridePendingTransition(0,0);
            }
        });

        layoutTabThongKe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PatientStatisticActivity.class));
                overridePendingTransition(0,0);
            }
        });

        layoutTabChanDoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PatientDiagnoseActivity.class));
                overridePendingTransition(0,0);
            }
        });
    }
}
