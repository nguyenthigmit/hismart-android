package com.his2018.hismart.activity;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.TextView;

import com.his2018.hismart.R;
import com.his2018.hismart.adapter.PatientInfoSharedAdapter;
import com.his2018.hismart.helper.Helper;
import com.his2018.hismart.model.PatientInfoShared;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PatientInfoSharedListActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)                 Toolbar toolbar;
    @BindView(R.id.txtTitleToolbar)         TextView txtTitleToolbar;
    @BindView(R.id.edtSearch)               EditText edtSearch;
    @BindView(R.id.rvPatientInfoShared)     RecyclerView rvPatientInfoShared;

    int width, height;

    ArrayList<PatientInfoShared> infoShareds;
    PatientInfoSharedAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_info_shared_list);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        txtTitleToolbar.setText(getResources().getString(R.string.danh_sach_benh_nhan_chia_se));

        infoShareds = new ArrayList<>();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(
                getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL);
        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.divider_recyclerview);
        dividerItemDecoration.setDrawable(drawable);
        rvPatientInfoShared.addItemDecoration(dividerItemDecoration);
        rvPatientInfoShared.setLayoutManager(layoutManager);
        rvPatientInfoShared.setHasFixedSize(true);
        rvPatientInfoShared.setItemAnimator(new DefaultItemAnimator());
        adapter = new PatientInfoSharedAdapter(this, infoShareds);
        rvPatientInfoShared.setAdapter(adapter);

        loadData();

        width   = Helper.getWidthScreen(this);
        height  = Helper.getHeightScreen(this);

        // set corner radius edittext
        GradientDrawable shape =  new GradientDrawable();
        shape.setCornerRadius(height/30);
        if(Build.VERSION.SDK_INT >= 23){
            shape.setColor(getResources().getColor(R.color.color_gray_bg, null));
        }else {
            shape.setColor(getResources().getColor(R.color.color_gray_bg));
        }
        edtSearch.setBackground(shape);

    }

    private void loadData() {
        Calendar calendar = Calendar.getInstance();
        infoShareds.add(new PatientInfoShared("Nguyễn Văn A", "Lê Tiến Minh Bác Sĩ", "Theo dõi huyết áp thường xuyên, có dấu hiệu cao huyết áp.", 1, calendar.getTime()));
        infoShareds.add(new PatientInfoShared("Nguyễn Văn A", "Lê Tiến Minh Bác Sĩ", "", 2, calendar.getTime()));
        infoShareds.add(new PatientInfoShared("Nguyễn Văn A", "Lê Tiến Minh Bác Sĩ", "", 1, calendar.getTime()));
        infoShareds.add(new PatientInfoShared("Nguyễn Văn A", "Lê Tiến Minh Bác Sĩ", "Theo dõi huyết áp thường xuyên, có dấu hiệu cao huyết áp.", 3, calendar.getTime()));
        infoShareds.add(new PatientInfoShared("Nguyễn Văn A", "Lê Tiến Minh Bác Sĩ", "Theo dõi huyết áp thường xuyên, có dấu hiệu cao huyết áp.", 3, calendar.getTime()));
        infoShareds.add(new PatientInfoShared("Nguyễn Văn A", "Lê Tiến Minh Bác Sĩ", "", 1, calendar.getTime()));
        infoShareds.add(new PatientInfoShared("Nguyễn Văn A", "Lê Tiến Minh Bác Sĩ", "", 2, calendar.getTime()));
        adapter.notifyDataSetChanged();
    }
}
