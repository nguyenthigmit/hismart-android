package com.his2018.hismart.activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.his2018.hismart.R;
import com.his2018.hismart.adapter.BloodPressureAlarmAdapter;
import com.his2018.hismart.model.BloodPressureAlarm;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BloodPressureAlarmActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)             Toolbar toolbar;
    @BindView(R.id.txtTitleToolbar)     TextView txtTitleToolbar;

    @BindView(R.id.rvBloodPressureAlarm)    RecyclerView rvBloodPressureAlarm;

    int width, height;

    ArrayList<BloodPressureAlarm> alarms;
    BloodPressureAlarmAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blood_pressure_alarm);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        txtTitleToolbar.setText(getResources().getString(R.string.bao_dong_huyet_ap, 5));

        alarms = new ArrayList<>();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(
                getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL);
        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.divider_recyclerview);
        dividerItemDecoration.setDrawable(drawable);
        rvBloodPressureAlarm.addItemDecoration(dividerItemDecoration);
        rvBloodPressureAlarm.setLayoutManager(layoutManager);
        rvBloodPressureAlarm.setHasFixedSize(true);
        rvBloodPressureAlarm.setItemAnimator(new DefaultItemAnimator());
        adapter = new BloodPressureAlarmAdapter(this, alarms);
        rvBloodPressureAlarm.setAdapter(adapter);

        loadData();
    }

    private void loadData() {
        Calendar calendar = Calendar.getInstance();
        alarms.add(new BloodPressureAlarm(1, "Nguyễn Lê Toàn Minh", 1, "43/290", calendar.getTime()));
        alarms.add(new BloodPressureAlarm(1, "Nguyễn Lê Toàn Minh", 2, "43/290", calendar.getTime()));
        alarms.add(new BloodPressureAlarm(1, "Nguyễn Lê Toàn Minh", 2, "43/290", calendar.getTime()));
        alarms.add(new BloodPressureAlarm(1, "Nguyễn Lê Toàn Minh", 3, "43/290", calendar.getTime()));
        alarms.add(new BloodPressureAlarm(1, "Nguyễn Lê Toàn Minh", 2, "43/290", calendar.getTime()));
        alarms.add(new BloodPressureAlarm(1, "Nguyễn Lê Toàn Minh", 3, "43/290", calendar.getTime()));
        alarms.add(new BloodPressureAlarm(1, "Nguyễn Lê Toàn Minh", 1, "43/290", calendar.getTime()));
        alarms.add(new BloodPressureAlarm(1, "Nguyễn Lê Toàn Minh", 2, "43/290", calendar.getTime()));
        adapter.notifyDataSetChanged();

    }
}
