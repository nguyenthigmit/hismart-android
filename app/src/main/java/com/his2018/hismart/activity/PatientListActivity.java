package com.his2018.hismart.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.his2018.hismart.R;
import com.his2018.hismart.adapter.PatientListAdapter;
import com.his2018.hismart.helper.Helper;
import com.his2018.hismart.model.DoctorResponse;
import com.his2018.hismart.model.PatientByDoctor;
import com.his2018.hismart.rest.ApiClient;
import com.his2018.hismart.util.Constant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PatientListActivity extends AppCompatActivity {


    @BindView(R.id.layoutToolbar)       LinearLayout layoutToolbar;
    @BindView(R.id.imgMainMenu)         ImageView imgMainMenu;
    @BindView(R.id.txtTitleToolbar)     TextView txtTitleToolbar;
    @BindView(R.id.imgSortAZ)           ImageView imgSortAZ;
    @BindView(R.id.edtSearch)           EditText edtSearch;

    @BindView(R.id.drawer_layout)       DrawerLayout drawerLayout;
    @BindView(R.id.navigationView)      NavigationView navigationView;
    @BindView(R.id.layoutHeaderNavi)    LinearLayout layoutHeaderNavi;
    @BindView(R.id.txtHISmart)          TextView txtHISmart;
    @BindView(R.id.txtViewer)           TextView txtViewer;
    @BindView(R.id.layoutInfo)          RelativeLayout layoutInfo;
    @BindView(R.id.imgAccount)          ImageView imgAccount;
    @BindView(R.id.txtHoTenBacSi)       TextView txtHoTenBacSi;
    @BindView(R.id.txtMaSoBacSi)        TextView txtMaSoBacSi;
    @BindView(R.id.layoutMainMenu)              LinearLayout layoutMainMenu;
    @BindView(R.id.layoutThongTinTaiKhoan)      LinearLayout layoutThongTinTaiKhoan;
    @BindView(R.id.imgThongTinTaiKhoan)         ImageView imgThongTinTaiKhoan;
    @BindView(R.id.txtThongTinTaiKhoan)         TextView txtThongTinTaiKhoan;
    @BindView(R.id.layoutDanhSachBenhNhan)      LinearLayout layoutDanhSachBenhNhan;
    @BindView(R.id.imgDanhSachBenhNhan)         ImageView imgDanhSachBenhNhan;
    @BindView(R.id.txtDanhSachBenhNhan)         TextView txtDanhSachBenhNhan;
    @BindView(R.id.layoutHopThuTinNhan)         LinearLayout layoutHopThuTinNhan;
    @BindView(R.id.imgHopThuTinNhan)            ImageView imgHopThuTinNhan;
    @BindView(R.id.txtHopThuTinNhan)            TextView txtHopThuTinNhan;
    @BindView(R.id.layoutDanhSachDaChiaSe)      LinearLayout layoutDanhSachDaChiaSe;
    @BindView(R.id.imgDanhSachDaChiaSe)         ImageView imgDanhSachDaChiaSe;
    @BindView(R.id.txtDanhSachDaChiaSe)         TextView txtDanhSachDaChiaSe;
    @BindView(R.id.layoutDanhSachDuocChiaSe)    LinearLayout layoutDanhSachDuocChiaSe;
    @BindView(R.id.imgDanhSachDuocChiaSe)       ImageView imgDanhSachDuocChiaSe;
    @BindView(R.id.txtDanhSachDuocChiaSe)       TextView txtDanhSachDuocChiaSe;
    @BindView(R.id.layoutDangXuat)              LinearLayout layoutDangXuat;
    @BindView(R.id.imgDangXuat)                 ImageView imgDangXuat;
    @BindView(R.id.txtDangXuat)                 TextView txtDangXuat;

    @BindView(R.id.layoutHeaderPatientList)     LinearLayout layoutHeaderPatientList;
    @BindView(R.id.txtSTT)                      TextView txtSTT;
    @BindView(R.id.txtMaSoBN)                   TextView txtMaSoBN;
    @BindView(R.id.txtThongTinBN)               TextView txtThongTinBN;
    @BindView(R.id.txtSoDienThoai)              TextView txtSoDienThoai;
    @BindView(R.id.rvPatient)                   RecyclerView rvPatient;


    int width, height;

    ArrayList<PatientByDoctor> arrayPatient;
    PatientListAdapter adapter;

    private int ORIENTATION = 1; // 1: PORTRAIT - 2: LANDSCAPE
    DoctorResponse doctor = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_list);

        ButterKnife.bind(this);

        doctor = Constant.DOCTOR_LOGIN;
        if (doctor != null){
            txtHoTenBacSi.setText(doctor.getFullName());
            txtMaSoBacSi.setText(doctor.getUserID());
        }else {
            finish();
            Toast.makeText(this, "DOCTOR NULL", Toast.LENGTH_SHORT).show();
            return;
        }

        setupRecyclerViewPatient();

        Configuration configuration = getResources().getConfiguration();
        if (configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
            setLayoutOrientationPortrait();
        }else {
            setLayoutOrientationLandscape();
        }

//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawerLayout.addDrawerListener(toggle);
//        toggle.syncState();

        imgMainMenu.setColorFilter(ContextCompat.getColor(this, R.color.color_blue_dark), PorterDuff.Mode.MULTIPLY);

        addEvent();


        loadPatientListByDoctor();

    }

    private void setupRecyclerViewPatient() {
        arrayPatient = new ArrayList<>();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(
                getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL);
        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.divider_recyclerview);
        dividerItemDecoration.setDrawable(drawable);
        rvPatient.addItemDecoration(dividerItemDecoration);
        rvPatient.setLayoutManager(layoutManager);
        rvPatient.setHasFixedSize(true);
        rvPatient.setItemAnimator(new DefaultItemAnimator());
        adapter = new PatientListAdapter(this, arrayPatient, ORIENTATION);
        rvPatient.setAdapter(adapter);
    }

    private void loadPatientListByDoctor() {
        ApiClient.getClient().getPatientByDoctor(doctor.getUserID())
                .enqueue(new Callback<List<PatientByDoctor>>() {
                    @Override
                    public void onResponse(Call<List<PatientByDoctor>> call, Response<List<PatientByDoctor>> response) {
                        if (response.body().size() > 0){
                            arrayPatient.addAll(response.body());
                            adapter = new PatientListAdapter(PatientListActivity.this, arrayPatient, ORIENTATION);
                            rvPatient.setAdapter(adapter);
                        }else {
                            Log.d("AAA", "ERROR LOAD PATIENTS");
                        }
                    }

                    @Override
                    public void onFailure(Call<List<PatientByDoctor>> call, Throwable t) {
                        Toast.makeText(PatientListActivity.this, R.string.loi_tai_danh_sach_benh_nhan, Toast.LENGTH_SHORT).show();
                        Log.d("AAA", "LOAD PATIENT FAIL: " + t.getMessage());
                    }
                });
    }

    private void addEvent() {
        imgSortAZ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Collections.sort(arrayPatient, new Comparator<PatientByDoctor>() {
                    public int compare(PatientByDoctor pa1, PatientByDoctor pa2) {
                        return pa1.getPatient_Name().compareTo(pa2.getPatient_Name());
                    }
                });
                adapter.notifyDataSetChanged();
            }
        });

        imgMainMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(Gravity.START);
            }
        });

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.filter(s.toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        /*
         * START ACTION IN MENU NAVIGATIONVIEW CUSTOM
         * */
        layoutThongTinTaiKhoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), AccountInfomationActivity.class));
                drawerLayout.closeDrawer(Gravity.START);
            }
        });

        layoutDanhSachBenhNhan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), PatientListActivity.class));
                drawerLayout.closeDrawer(Gravity.START);
            }
        });

        layoutDanhSachDaChiaSe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), PatientInfoSharedListActivity.class));
                drawerLayout.closeDrawer(Gravity.START);
            }
        });

        /*
         * END ACTION IN MENU NAVIGATIONVIEW CUSTOM
         * */
    }

    private void setLayoutViewVertical() {
        /*
        * Toolbar
        * */
        int paddingTB5 = height/50;
        int paddingLR15 = width/30;
        layoutToolbar.setPadding(paddingLR15, paddingTB5, paddingLR15, paddingTB5);

        int width30 = width/20;
        LinearLayout.LayoutParams paramsMainMenu = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, width30);
        imgMainMenu.setLayoutParams(paramsMainMenu);
        imgMainMenu.setAdjustViewBounds(true);

        int text_size = height/35;
        txtTitleToolbar.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);

        imgSortAZ.setLayoutParams(paramsMainMenu);
        imgSortAZ.setAdjustViewBounds(true);

        int height40 = height/14;
        int marginLR15 = width/30;
        int marginB5 = height/50;
        LinearLayout.LayoutParams paramsSearch = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height40);
        paramsSearch.setMargins(marginLR15, 0, marginLR15, marginB5);
        edtSearch.setLayoutParams(paramsSearch);
        edtSearch.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);

        // set corner radius edittext
        GradientDrawable shape =  new GradientDrawable();
        shape.setCornerRadius(height/30);
        if(Build.VERSION.SDK_INT >= 23){
            shape.setColor(getResources().getColor(R.color.color_white, null));
        }else {
            shape.setColor(getResources().getColor(R.color.color_white));
        }
        edtSearch.setBackground(shape);

        /*
         * Set width NavigationView
         * */
        DrawerLayout.LayoutParams paramsNavi = new DrawerLayout.LayoutParams(width*4/5, ViewGroup.LayoutParams.MATCH_PARENT);
        paramsNavi.gravity = GravityCompat.START;
        navigationView.setLayoutParams(paramsNavi);

        /*
        * Header navi
        * */
        int paddingLR20 = width/25;
        layoutHeaderNavi.setPadding(paddingLR20, 0, paddingLR20, 0);

        float textSizeHIS = height/25;
        txtHISmart.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeHIS);

        float textSizeViewer = height/40;
        txtViewer.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeViewer);

        int paddingT10 = height/35;
        layoutInfo.setPadding(0, paddingT10, 0, 0);
        int marginB10 = height/35;
        int marginR20 = width/25;
        RelativeLayout.LayoutParams paramsAccount = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsAccount.setMargins(0, 0, marginR20, marginB10);
        imgAccount.setLayoutParams(paramsAccount);
        imgAccount.setAdjustViewBounds(true);
        imgAccount.setMaxHeight(height40);
        txtHoTenBacSi.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        txtMaSoBacSi.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);

        /*
        * MENU NAVI
        * */
        int padding20 = height/30;
        int paddingR40 = width/15;
        layoutMainMenu.setPadding(padding20, padding20, padding20, padding20);

        int height30 = height/20;
        layoutThongTinTaiKhoan.setPadding(0, paddingTB5, 0, paddingTB5);
        LinearLayout.LayoutParams paramsMenuIcon = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, height30);
        imgThongTinTaiKhoan.setLayoutParams(paramsMenuIcon);
        imgThongTinTaiKhoan.setPadding(0, 0, paddingR40, 0);
        imgThongTinTaiKhoan.setAdjustViewBounds(true);
        txtThongTinTaiKhoan.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);

        layoutDanhSachBenhNhan.setPadding(0, paddingTB5, 0, paddingTB5);
        imgDanhSachBenhNhan.setLayoutParams(paramsMenuIcon);
        imgDanhSachBenhNhan.setPadding(0, 0, paddingR40, 0);
        imgDanhSachBenhNhan.setAdjustViewBounds(true);
        txtDanhSachBenhNhan.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);

        layoutHopThuTinNhan.setPadding(0, paddingTB5, 0, paddingTB5);
        imgHopThuTinNhan.setLayoutParams(paramsMenuIcon);
        imgHopThuTinNhan.setPadding(0, 0, paddingR40, 0);
        imgHopThuTinNhan.setAdjustViewBounds(true);
        txtHopThuTinNhan.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);

        layoutDanhSachDaChiaSe.setPadding(0, paddingTB5, 0, paddingTB5);
        imgDanhSachDaChiaSe.setLayoutParams(paramsMenuIcon);
        imgDanhSachDaChiaSe.setPadding(0, 0, paddingR40, 0);
        imgDanhSachDaChiaSe.setAdjustViewBounds(true);
        txtDanhSachDaChiaSe.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);

        layoutDanhSachDuocChiaSe.setPadding(0, paddingTB5, 0, paddingTB5);
        imgDanhSachDuocChiaSe.setLayoutParams(paramsMenuIcon);
        imgDanhSachDuocChiaSe.setPadding(0, 0, paddingR40, 0);
        imgDanhSachDuocChiaSe.setAdjustViewBounds(true);
        txtDanhSachDuocChiaSe.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);

        layoutDangXuat.setPadding(0, paddingTB5, 0, paddingTB5);
        imgDangXuat.setLayoutParams(paramsMenuIcon);
        imgDangXuat.setPadding(0, 0, paddingR40, 0);
        imgDangXuat.setAdjustViewBounds(true);
        txtDangXuat.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);

        /*
        * HEADER LIST PATIENT
        * */
        int padding10 = height/70;
        layoutHeaderPatientList.setPadding(padding10, padding10, padding10, padding10);

        float textSizeHeaderPatient = height/40;
        txtSTT.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeHeaderPatient);
        LinearLayout.LayoutParams paramsSTT = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.5f);
        txtSTT.setLayoutParams(paramsSTT);

        txtMaSoBN.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeHeaderPatient);
        LinearLayout.LayoutParams paramsMaBN = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 5.5f);
        txtMaSoBN.setLayoutParams(paramsMaBN);

        txtThongTinBN.setVisibility(View.GONE);

        txtSoDienThoai.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeHeaderPatient);
        LinearLayout.LayoutParams paramsSoDT = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 3f);
        txtSoDienThoai.setLayoutParams(paramsSoDT);
    }

    private void setLayoutViewHorizontal() {
        /*
        * Toolbar
        * */
        int paddingTB5 = height/40;
        int paddingLR15 = width/35;
        layoutToolbar.setPadding(paddingLR15, paddingTB5, paddingLR15, paddingTB5);

        int width30 = width/30;
        LinearLayout.LayoutParams paramsMainMenu = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, width30);
        imgMainMenu.setLayoutParams(paramsMainMenu);
        imgMainMenu.setAdjustViewBounds(true);

        int text_size = height/25;
        txtTitleToolbar.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);

        imgSortAZ.setLayoutParams(paramsMainMenu);
        imgSortAZ.setAdjustViewBounds(true);

        int height40_Search = height/11;
        int marginLR15 = width/35;
        int marginB5 = height/45;
        int textSizeSearch = height/35;
        LinearLayout.LayoutParams paramsSearch = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height40_Search);
        paramsSearch.setMargins(marginLR15, 0, marginLR15, marginB5);
        edtSearch.setLayoutParams(paramsSearch);
        edtSearch.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeSearch);

        // set corner radius edittext
        GradientDrawable shape =  new GradientDrawable();
        shape.setCornerRadius(height/20);
        if(Build.VERSION.SDK_INT >= 23){
            shape.setColor(getResources().getColor(R.color.color_white, null));
        }else {
            shape.setColor(getResources().getColor(R.color.color_white));
        }
        edtSearch.setBackground(shape);


        /*
        * Set width NavigationView
        * */
        DrawerLayout.LayoutParams paramsNavi = new DrawerLayout.LayoutParams(width*3/5, ViewGroup.LayoutParams.MATCH_PARENT);
        paramsNavi.gravity = GravityCompat.START;
        navigationView.setLayoutParams(paramsNavi);

        /*
        * Header navi
        * */
        int paddingLR20 = width/35;
        layoutHeaderNavi.setPadding(paddingLR20, 0, paddingLR20, 0);
        float textSizeHIS = height/15;
        txtHISmart.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeHIS);
        float textSizeViewer = height/30;
        txtViewer.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeViewer);
        int paddingT10 = height/35;
        layoutInfo.setPadding(0, paddingT10, 0, 0);
        int marginB10 = height/25;
        int marginR20 = width/35;
        int text_size_navi = height/20;
        int height40 = height/8;
        RelativeLayout.LayoutParams paramsAccount = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsAccount.setMargins(0, 0, marginR20, marginB10);
        imgAccount.setLayoutParams(paramsAccount);
        imgAccount.setAdjustViewBounds(true);
        imgAccount.setMaxHeight(height40);
        txtHoTenBacSi.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_navi);
        txtMaSoBacSi.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_navi);

        /*
        * MENU NAVI
        * */
        int padding20 = height/20;
        int paddingR40 = width/25;
        layoutMainMenu.setPadding(padding20, padding20, padding20, padding20);

        int height30 = height/15;
        layoutThongTinTaiKhoan.setPadding(0, paddingTB5, 0, paddingTB5);
        LinearLayout.LayoutParams paramsMenuIcon = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, height30);
        imgThongTinTaiKhoan.setLayoutParams(paramsMenuIcon);
        imgThongTinTaiKhoan.setPadding(0, 0, paddingR40, 0);
        imgThongTinTaiKhoan.setAdjustViewBounds(true);
        txtThongTinTaiKhoan.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_navi);

        layoutDanhSachBenhNhan.setPadding(0, paddingTB5, 0, paddingTB5);
        imgDanhSachBenhNhan.setLayoutParams(paramsMenuIcon);
        imgDanhSachBenhNhan.setPadding(0, 0, paddingR40, 0);
        imgDanhSachBenhNhan.setAdjustViewBounds(true);
        txtDanhSachBenhNhan.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_navi);

        layoutHopThuTinNhan.setPadding(0, paddingTB5, 0, paddingTB5);
        imgHopThuTinNhan.setLayoutParams(paramsMenuIcon);
        imgHopThuTinNhan.setPadding(0, 0, paddingR40, 0);
        imgHopThuTinNhan.setAdjustViewBounds(true);
        txtHopThuTinNhan.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_navi);

        layoutDanhSachDaChiaSe.setPadding(0, paddingTB5, 0, paddingTB5);
        imgDanhSachDaChiaSe.setLayoutParams(paramsMenuIcon);
        imgDanhSachDaChiaSe.setPadding(0, 0, paddingR40, 0);
        imgDanhSachDaChiaSe.setAdjustViewBounds(true);
        txtDanhSachDaChiaSe.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_navi);

        layoutDanhSachDuocChiaSe.setPadding(0, paddingTB5, 0, paddingTB5);
        imgDanhSachDuocChiaSe.setLayoutParams(paramsMenuIcon);
        imgDanhSachDuocChiaSe.setPadding(0, 0, paddingR40, 0);
        imgDanhSachDuocChiaSe.setAdjustViewBounds(true);
        txtDanhSachDuocChiaSe.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_navi);

        layoutDangXuat.setPadding(0, paddingTB5, 0, paddingTB5);
        imgDangXuat.setLayoutParams(paramsMenuIcon);
        imgDangXuat.setPadding(0, 0, paddingR40, 0);
        imgDangXuat.setAdjustViewBounds(true);
        txtDangXuat.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_navi);

        /*
        * HEADER LIST PATIENT
        * */
        int padding10 = height/45;
        layoutHeaderPatientList.setPadding(padding10, padding10, padding10, padding10);

        float textSizeHeaderPatient = height/25;
        txtSTT.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeHeaderPatient);
        LinearLayout.LayoutParams paramsSTT = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
        txtSTT.setLayoutParams(paramsSTT);

        txtMaSoBN.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeHeaderPatient);
        LinearLayout.LayoutParams paramsMaBN = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 2.5f);
        txtMaSoBN.setLayoutParams(paramsMaBN);

        txtThongTinBN.setVisibility(View.VISIBLE);
        txtThongTinBN.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeHeaderPatient);
        LinearLayout.LayoutParams paramsThongTinBN = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 4f);
        txtThongTinBN.setLayoutParams(paramsThongTinBN);

        txtSoDienThoai.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeHeaderPatient);
        LinearLayout.LayoutParams paramsSoDT = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 2.5f);
        txtSoDienThoai.setLayoutParams(paramsSoDT);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void setLayoutOrientationPortrait(){
        width   = Helper.getWidthScreen(this);
        height  = Helper.getHeightScreen(this);
        setLayoutViewVertical();
        ORIENTATION = 1;
    }

    private void setLayoutOrientationLandscape(){
        width   = Helper.getWidthScreen(this);
        height  = Helper.getHeightScreen(this);
        setLayoutViewHorizontal();
        ORIENTATION = 2;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setLayoutOrientationLandscape();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            setLayoutOrientationPortrait();
        }
        // show list patients 2  type by screen orientation
        adapter = new PatientListAdapter(this, arrayPatient, ORIENTATION);
        rvPatient.setAdapter(adapter);
    }
}
