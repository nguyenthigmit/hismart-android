package com.his2018.hismart.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.his2018.hismart.R;
import com.his2018.hismart.adapter.DoctorShareAdapter;
import com.his2018.hismart.model.DoctorResponse;
import com.his2018.hismart.model.PatientByDoctor;
import com.his2018.hismart.rest.ApiClient;
import com.his2018.hismart.util.Constant;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SharePatientInfoActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)             Toolbar toolbar;
    @BindView(R.id.txtTitleToolbar)     TextView txtTitleToolbar;
    @BindView(R.id.txtActionToolbar)    TextView txtActionToolbar;
    @BindView(R.id.txtShareTitle)       TextView txtShareTitle;
    @BindView(R.id.autoCompleteDoctor)  AutoCompleteTextView autoCompleteDoctor;

    ArrayList<DoctorResponse> arrayDoctor;
    DoctorShareAdapter adapterDoctor;

    PatientByDoctor PATIENT = null;
    String USER_ID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_patient_info);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        txtTitleToolbar.setText(R.string.share_ho_so_thong_tin);
        txtActionToolbar.setVisibility(View.VISIBLE);
        txtActionToolbar.setText(R.string.gui_);

        PATIENT = Constant.PATIENT_BY_DOCTOR;

        loadDoctorShareByPatient();
        addEvent();
    }

    private void loadDoctorShareByPatient() {
        ApiClient.getClient().getDoctorShareByPatient(PATIENT.getPID_ID())
                .enqueue(new Callback<List<DoctorResponse>>() {
                    @Override
                    public void onResponse(Call<List<DoctorResponse>> call, Response<List<DoctorResponse>> response) {
                        arrayDoctor = new ArrayList<>();
                        for (DoctorResponse doctor : response.body()){
                            arrayDoctor.add(doctor);
                        }
                        // AutoCompleteTextView using adapter not work with adapterDoctor.notifyDataSetChanged();
                        // set inner this function after load success
                        adapterDoctor = new DoctorShareAdapter(SharePatientInfoActivity.this, R.layout.layout_row_auto_complete, arrayDoctor);
                        autoCompleteDoctor.setAdapter(adapterDoctor);
                        autoCompleteDoctor.setThreshold(1);
                    }

                    @Override
                    public void onFailure(Call<List<DoctorResponse>> call, Throwable t) {
                        Toast.makeText(SharePatientInfoActivity.this, getString(R.string.xay_ra_loi), Toast.LENGTH_SHORT).show();
                        Log.d("AAA", "Load Doctor share by patient Fail: " + t.getMessage());
                    }
                });
    }

    private void addEvent() {

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        txtActionToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!USER_ID.trim().isEmpty()){
                    sharePatient();
                }
            }
        });

        autoCompleteDoctor.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DoctorResponse doctorResponse = (DoctorResponse) parent.getItemAtPosition(position);
                USER_ID = doctorResponse.getUserID();
            }
        });

//        txtActionToolbar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Dialog dialog = new Dialog(SharePatientInfoActivity.this);
//                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                dialog.setContentView(R.layout.layout_dialog_share_patient_info);
//                dialog.show();
//            }
//        });
    }

    private void sharePatient() {
        ApiClient.getClient().sharePatientInfoByDoctor(PATIENT.getPID_ID(), USER_ID)
                .enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        Log.d("AAA", "SHARE PATIENT RESPONSE: " + response.body());
                        if (response.body().equals("success")){
                            Toast.makeText(SharePatientInfoActivity.this, getString(R.string.chia_se_thanh_cong), Toast.LENGTH_SHORT).show();
//                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Log.d("AAA", "SHARE PATIENT FAIL...");
                        Toast.makeText(SharePatientInfoActivity.this, getString(R.string.xay_ra_loi), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
