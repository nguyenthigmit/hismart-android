package com.his2018.hismart.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.his2018.hismart.R;
import com.his2018.hismart.model.AddressPresponse;
import com.his2018.hismart.model.PatientByDoctor;
import com.his2018.hismart.model.PatientInfoResponse;
import com.his2018.hismart.rest.ApiClient;
import com.his2018.hismart.util.Constant;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PatientInfomationActivity extends AppCompatActivity {


    @BindView(R.id.toolbar)             Toolbar toolbar;
    @BindView(R.id.txtTitleToolbar)     TextView txtTitleToolbar;
    @BindView(R.id.layoutHoTen)         LinearLayout layoutHoTen;
    @BindView(R.id.txtHoTenBN)          TextView txtHoTenBN;
    @BindView(R.id.txtMaSoBN)           TextView txtMaSoBN;
    @BindView(R.id.txtBsTheoDoiChinh)   TextView txtBsTheoDoiChinh;
    @BindView(R.id.txtBsTheoDoiPhu)     TextView txtBsTheoDoiPhu;
    @BindView(R.id.layoutChiTiet)       LinearLayout layoutChiTiet;
    @BindView(R.id.txtGioiTinh)         TextView txtGioiTinh;
    @BindView(R.id.txtNgaySinh)         TextView txtNgaySinh;
    @BindView(R.id.txtTuoi)             TextView txtTuoi;
    @BindView(R.id.txtSoDienThoai)      TextView txtSoDienThoai;
    @BindView(R.id.txtDiaChi)           TextView txtDiaChi;
    @BindView(R.id.layoutNhomMau)       LinearLayout layoutNhomMau;
    @BindView(R.id.txtNhomMau)          TextView txtNhomMau;
    @BindView(R.id.txtMasoThietBi)      TextView txtMasoThietBi;
    @BindView(R.id.layoutThongSoHuyetAp)    LinearLayout layoutThongSoHuyetAp;
    @BindView(R.id.txtThongSoTitle)     TextView txtThongSoTitle;
    @BindView(R.id.imgEdit)             ImageView imgEdit;
    @BindView(R.id.txtLowSystolicTitle) TextView txtLowSystolicTitle;
    @BindView(R.id.txtLowSystolic)      TextView txtLowSystolic;
    @BindView(R.id.txtPreHighSystolicTitle) TextView txtPreHighSystolicTitle;
    @BindView(R.id.txtPreHighSystolic)      TextView txtPreHighSystolic;
    @BindView(R.id.txtHighSystolicTitle)    TextView txtHighSystolicTitle;
    @BindView(R.id.txtHighSystolic)         TextView txtHighSystolic;
    @BindView(R.id.txtLowDiastolicTitle)    TextView txtLowDiastolicTitle;
    @BindView(R.id.txtLowDiastolic)         TextView txtLowDiastolic;
    @BindView(R.id.txtPreHighDiastolicTitle)    TextView txtPreHighDiastolicTitle;
    @BindView(R.id.txtPreHighDiastolic)     TextView txtPreHighDiastolic;
    @BindView(R.id.txtHighDiastolicTitle)   TextView txtHighDiastolicTitle;
    @BindView(R.id.txtHighDiastolic)        TextView txtHighDiastolic;

    PatientInfoResponse PATIENT_INFO = null;
    int REQUEST_UPDATE = 159;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_infomation);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        txtTitleToolbar.setText(R.string.thong_tin_benh_nhan);

        loadBloodPressureResultPatient();
        addEvent();

    }

    private void loadBloodPressureResultPatient() {
        String PIP_ID = Constant.PATIENT_BY_DOCTOR.getPID_ID();
        ApiClient.getClient().getBloodPressureByPipId(PIP_ID)
                .enqueue(new Callback<List<PatientInfoResponse>>() {
                    @Override
                    public void onResponse(Call<List<PatientInfoResponse>> call, Response<List<PatientInfoResponse>> response) {
                        if (response.body() != null){
                            if (response.body().size() > 0){
                                PATIENT_INFO = response.body().get(0);
                                if (PATIENT_INFO != null){
                                    setInfo();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<List<PatientInfoResponse>> call, Throwable t) {
                        Log.d("AAA", "LOAD PATIENT INFO BY PIP ID FAIL ");
                        Toast.makeText(PatientInfomationActivity.this, getString(R.string.xay_ra_loi), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
    }

    private void setInfo() {
        PatientByDoctor patientByDoctor = Constant.PATIENT_BY_DOCTOR;
        txtHoTenBN.setText(patientByDoctor.getPatient_Name());
        txtMaSoBN.setText(patientByDoctor.getPID_ID());
        txtBsTheoDoiChinh.setText("Bs theo dõi chính: " + Constant.DOCTOR_LOGIN.getFullName());
        txtBsTheoDoiPhu.setText("Bs theo dõi phụ: ");
        String gioiTinh = patientByDoctor.getSex_ID() == 0 ? "Nam" : "Nữ";
        txtGioiTinh.setText("Giới tính: " + gioiTinh);
        SimpleDateFormat simpleDateFormat   = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat dateFormat         = new SimpleDateFormat("dd/MM/yyyy");
        // convert string to Date
        try {
            Date date = simpleDateFormat.parse(patientByDoctor.getBirth_Date());
            String birthDay = dateFormat.format(date);
            txtNgaySinh.setText("Ngày sinh: " + birthDay);
            // cal year old
            Calendar calendarNow = Calendar.getInstance();
            Calendar calendarPA = Calendar.getInstance();
            calendarPA.setTime(date);
            int age = calendarNow.get(Calendar.YEAR) - calendarPA.get(Calendar.YEAR);
            txtTuoi.setText("Tuổi: " + age);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        txtSoDienThoai.setText("SĐT: " + patientByDoctor.getMobile_Phone());
        getPatientAddress(patientByDoctor.getAddress_ID());
        txtNhomMau.setText("Nhóm máu: " + PATIENT_INFO.getBloodType());
        txtMasoThietBi.setText("Mã số thiết bị y tế: "  + patientByDoctor.getIdCard_Number());
        txtLowSystolic.setText(String.valueOf(PATIENT_INFO.getLowSystolic()));
        txtPreHighSystolic.setText(String.valueOf(PATIENT_INFO.getPreHighSystolic()));
        txtHighSystolic.setText(String.valueOf(PATIENT_INFO.getHighSystolic()));
        txtLowDiastolic.setText(String.valueOf(PATIENT_INFO.getLowDiastolic()));
        txtPreHighDiastolic.setText(String.valueOf(PATIENT_INFO.getPreHighDiastolic()));
        txtHighDiastolic.setText(String.valueOf(PATIENT_INFO.getHighDiastolic()));
    }

    private void getPatientAddress(final int address_id) {
        ApiClient.getClient().loadAddress()
                .enqueue(new Callback<List<AddressPresponse>>() {
                    @Override
                    public void onResponse(Call<List<AddressPresponse>> call, Response<List<AddressPresponse>> response) {
                        for (AddressPresponse address : response.body()){
                            if (address.getAddress_ID() == address_id){
                                String addr  = address.getStreet() + ", " + address.getOther_Destination() + ", " + address.getCity();
                                txtDiaChi.setText("Địa chỉ: " + addr);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<List<AddressPresponse>> call, Throwable t) {
                        Log.d("AAA", "Load Address Fail: " + t.getMessage());
                    }
                });
    }

    private void addEvent(){

        imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PATIENT_INFO != null){
                    Intent intent = new Intent(getApplicationContext(), BloodPressureWarningEditActivity.class);
                    intent.putExtra("PATIENT_INFO", PATIENT_INFO);
                    startActivityForResult(intent, REQUEST_UPDATE);
                }
            }
        });

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_UPDATE && resultCode == RESULT_OK){
            loadBloodPressureResultPatient();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
