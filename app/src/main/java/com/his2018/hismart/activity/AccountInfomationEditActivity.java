package com.his2018.hismart.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.his2018.hismart.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AccountInfomationEditActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)     Toolbar toolbar;
    @BindView(R.id.txtTitleToolbar) TextView txtTitleToolbar;
    @BindView(R.id.txtActionToolbar)    TextView txtActionToolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_infomation_edit);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        txtTitleToolbar.setText(getResources().getString(R.string.thong_tin_tai_khoan));
        txtActionToolbar.setVisibility(View.VISIBLE);
        txtActionToolbar.setText(getResources().getString(R.string.luu_));
    }
}
