package com.his2018.hismart.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.his2018.hismart.R;
import com.his2018.hismart.model.AddressPresponse;
import com.his2018.hismart.model.DoctorResponse;
import com.his2018.hismart.model.Patient;
import com.his2018.hismart.rest.ApiClient;
import com.his2018.hismart.util.Constant;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccountInfomationActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)             Toolbar toolbar;
    @BindView(R.id.txtTitleToolbar)     TextView txtTitleToolbar;
    @BindView(R.id.fabEdit)             FloatingActionButton fabEdit;

    @BindView(R.id.txtAccountType)      TextView txtAccountType;
    @BindView(R.id.txtFullNameUser)     TextView txtFullNameUser;
    @BindView(R.id.txtIdUser)           TextView txtIdUser;
    @BindView(R.id.txtGenderUser)       TextView txtGenderUser;
    @BindView(R.id.txtBirthdayUser)     TextView txtBirthdayUser;
    @BindView(R.id.txtAgeUser)          TextView txtAgeUser;
    @BindView(R.id.txtMobilePhoneUser)  TextView txtMobilePhoneUser;
    @BindView(R.id.txtAddressUser)      TextView txtAddressUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_infomation);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        txtTitleToolbar.setText(getResources().getString(R.string.thong_tin_tai_khoan));

        setInformation();

        
        addEvent();
    }

    private void setInformation() {
        String accountType = Constant.IS_DOCTOR ? getResources().getString(R.string.tai_khoan_bac_si) : getResources().getString(R.string.tai_khoan_benh_nhan);
        txtAccountType.setText(accountType);

        if (Constant.IS_DOCTOR){
            DoctorResponse doctor = Constant.DOCTOR_LOGIN;
            if (doctor != null){
                txtFullNameUser.setText("BS. " +  doctor.getFullName());
                txtIdUser.setText(doctor.getUserID());
                String gender = doctor.getGender() == 0 ? getString(R.string.nam) : getString(R.string.nu);
                txtGenderUser.setText("Giới tính: " + gender);
                txtBirthdayUser.setVisibility(View.GONE);
                txtAgeUser.setVisibility(View.GONE);
                txtMobilePhoneUser.setText("Số điện thoại: " + doctor.getMobile_Phone());
                txtAddressUser.setText("Cơ sở y tế: " + doctor.getMedicalCenterID());
            }

        }else {
            Patient patient = Constant.PATIENT_LOGIN;
            if (patient != null){
                txtFullNameUser.setText("BN. " + patient.getPatient_Name());
                txtIdUser.setText(patient.getPID_ID());
                String gender = patient.getSex_ID().equals("0") ? getString(R.string.nam) : getString(R.string.nu);
                txtGenderUser.setText("Giới tính: " + gender);
                // set Birthday
                SimpleDateFormat simpleDateFormat   = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                SimpleDateFormat dateFormat         = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    Date date = simpleDateFormat.parse(patient.getBirth_Date());
                    String birthDay = dateFormat.format(date);
                    txtBirthdayUser.setText("Ngày sinh: " + birthDay);
                    Calendar calendarNow = Calendar.getInstance();
                    Calendar calendarUser = Calendar.getInstance();
                    calendarUser.setTime(date);
                    int age = calendarNow.get(Calendar.YEAR) - calendarUser.get(Calendar.YEAR);
                    txtAgeUser.setText("Tuổi: " + age);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                // set phone number
                txtMobilePhoneUser.setText("Số điện thoại: " + patient.getMobile_Phone());
                // set Address
                loadAddressUser(patient.getAddress_ID());
            }
        }

    }

    private void loadAddressUser(final int addressID) {
        ApiClient.getClient().loadAddress()
                .enqueue(new Callback<List<AddressPresponse>>() {
                    @Override
                    public void onResponse(Call<List<AddressPresponse>> call, Response<List<AddressPresponse>> response) {
                        for (AddressPresponse address : response.body()){
                            if (address.getAddress_ID() == addressID){
                                String addr  = address.getStreet() + ", " + address.getOther_Destination() + ", " + address.getCity();
                                txtAddressUser.setText("Địa chỉ: " + addr);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<List<AddressPresponse>> call, Throwable t) {
                        Log.d("AAA", "Load Address Fail: " + t.getMessage());
                    }
                });
    }

    private void addEvent() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        fabEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), AccountInfomationEditActivity.class));
            }
        });
    }
}
