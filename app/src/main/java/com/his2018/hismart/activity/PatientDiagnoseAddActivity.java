package com.his2018.hismart.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.his2018.hismart.R;
import com.his2018.hismart.helper.Helper;
import com.his2018.hismart.rest.ApiClient;
import com.his2018.hismart.util.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PatientDiagnoseAddActivity extends AppCompatActivity {


    // tab bottom
    @BindView(R.id.layoutTabDoThi)      LinearLayout layoutTabDoThi;
    @BindView(R.id.imgTabDoThi)         ImageView imgTabDoThi;
    @BindView(R.id.txtTabDoThi)         TextView txtTabDoThi;
    @BindView(R.id.layoutTabKetQua)     LinearLayout layoutTabKetQua;
    @BindView(R.id.imgTabKetQua)        ImageView imgTabKetQua;
    @BindView(R.id.txtTabKetQua)        TextView txtTabKetQua;
    @BindView(R.id.layoutTabThongKe)    LinearLayout layoutTabThongKe;
    @BindView(R.id.imgTabThongKe)       ImageView imgTabThongKe;
    @BindView(R.id.txtTabThongKe)       TextView txtTabThongKe;
    @BindView(R.id.layoutTabChanDoan)   LinearLayout layoutTabChanDoan;
    @BindView(R.id.imgTabChanDoan)      ImageView imgTabChanDoan;
    @BindView(R.id.txtTabChanDoan)      TextView txtTabChanDoan;
    @BindView(R.id.layoutTabDonThuoc)   LinearLayout layoutTabDonThuoc;
    @BindView(R.id.imgTabDonThuoc)      ImageView imgTabDonThuoc;
    @BindView(R.id.txtTabDonThuoc)      TextView txtTabDonThuoc;

    // view content
    @BindView(R.id.layoutToolbar)       RelativeLayout layoutToolbar;
    @BindView(R.id.imgCancel)           ImageView imgCancel;
    @BindView(R.id.txtChanDoanDieuTri)  TextView txtChanDoanDieuTri;
    @BindView(R.id.txtLuuChanDoan)      TextView txtLuuChanDoan;
    @BindView(R.id.layoutInput)         LinearLayout layoutInput;
    @BindView(R.id.layoutTenBenh)       LinearLayout layoutTenBenh;
    @BindView(R.id.txtTenBenh)          TextView txtTenBenh;
    @BindView(R.id.edtTenBenh)          EditText edtTenBenh;
    @BindView(R.id.layoutChanDoan)      LinearLayout layoutChanDoan;
    @BindView(R.id.txtChanDoan)         TextView txtChanDoan;
    @BindView(R.id.edtChanDoan)         EditText edtChanDoan;
    @BindView(R.id.layoutHuongXuLy)     LinearLayout layoutHuongXuLy;
    @BindView(R.id.txtHuongXuLy)        TextView txtHuongXuLy;
    @BindView(R.id.edtHuongXuLy)        EditText edtHuongXuLy;
    @BindView(R.id.layoutXuLy)          LinearLayout layoutXuLy;
    @BindView(R.id.txtXuLy)             TextView txtXuLy;
    @BindView(R.id.edtXuLy)             EditText edtXuLy;
    @BindView(R.id.layoutLoiDan)        LinearLayout layoutLoiDan;
    @BindView(R.id.txtLoiDan)           TextView txtLoiDan;
    @BindView(R.id.edtLoiDan)           EditText edtLoiDan;

    private int width, height;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_diagnose_add);

        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT < 23){
            layoutTabChanDoan.setBackgroundColor(getResources().getColor(R.color.color_tab_selected));
        }else {
            layoutTabChanDoan.setBackgroundColor(getColor(R.color.color_tab_selected));
        }

        Configuration configuration = getResources().getConfiguration();
        if (configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
            setLayoutOrientationPortrait();
        }else {
            setLayoutOrientationLandscape();
        }

        addEvent();
    }

    private void setLayoutOrientationPortrait(){
        width   = Helper.getWidthScreen(this);
        height  = Helper.getHeightScreen(this);
        setLayoutViewVertical();
    }

    private void setLayoutOrientationLandscape(){
        width   = Helper.getWidthScreen(this);
        height  = Helper.getHeightScreen(this);
        setLayoutViewHorizontal();
    }

    private void setLayoutViewVertical() {
        setToolbarPatientVertical();
        setTabBottomVertical();
    }

    private void setLayoutViewHorizontal() {
        setToolbarPatientHorizontal();
        setTabBottomHorizontal();
    }

    private void setToolbarPatientVertical(){

    }


    private void setTabBottomVertical(){
        /*
         * TAB BOTTOM
         * */
        int paddingTBTab = height/80; // 5dp
        int height30 = height/30;
        float textSizeTab = height/45;
        layoutTabDoThi.setPadding(0, paddingTBTab, 0, paddingTBTab);
        LinearLayout.LayoutParams paramsImageTab = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height30);
        imgTabDoThi.setLayoutParams(paramsImageTab);
        imgTabDoThi.setAdjustViewBounds(true);
        txtTabDoThi.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);

        layoutTabKetQua.setPadding(0, paddingTBTab, 0, paddingTBTab);
        imgTabKetQua.setLayoutParams(paramsImageTab);
        imgTabKetQua.setAdjustViewBounds(true);
        txtTabKetQua.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);

        layoutTabThongKe.setPadding(0, paddingTBTab, 0, paddingTBTab);
        imgTabThongKe.setLayoutParams(paramsImageTab);
        imgTabThongKe.setAdjustViewBounds(true);
        txtTabThongKe.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);

        layoutTabChanDoan.setPadding(0, paddingTBTab, 0, paddingTBTab);
        imgTabChanDoan.setLayoutParams(paramsImageTab);
        imgTabChanDoan.setAdjustViewBounds(true);
        txtTabChanDoan.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);

        layoutTabDonThuoc.setPadding(0, paddingTBTab, 0, paddingTBTab);
        imgTabDonThuoc.setLayoutParams(paramsImageTab);
        imgTabDonThuoc.setAdjustViewBounds(true);
        txtTabDonThuoc.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);
    }

    private void setToolbarPatientHorizontal(){

    }

    private void setTabBottomHorizontal(){
        /*
         * TAB BOTTOM
         * */
        int paddingTBTab = height/50; // 5dp
        int height30 = height/20;
        float textSizeTab = height/30;
        layoutTabDoThi.setPadding(0, paddingTBTab, 0, paddingTBTab);
        LinearLayout.LayoutParams paramsImageTab = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height30);
        imgTabDoThi.setLayoutParams(paramsImageTab);
        imgTabDoThi.setAdjustViewBounds(true);
        txtTabDoThi.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);

        layoutTabKetQua.setPadding(0, paddingTBTab, 0, paddingTBTab);
        imgTabKetQua.setLayoutParams(paramsImageTab);
        imgTabKetQua.setAdjustViewBounds(true);
        txtTabKetQua.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);

        layoutTabThongKe.setPadding(0, paddingTBTab, 0, paddingTBTab);
        imgTabThongKe.setLayoutParams(paramsImageTab);
        imgTabThongKe.setAdjustViewBounds(true);
        txtTabThongKe.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);

        layoutTabChanDoan.setPadding(0, paddingTBTab, 0, paddingTBTab);
        imgTabChanDoan.setLayoutParams(paramsImageTab);
        imgTabChanDoan.setAdjustViewBounds(true);
        txtTabChanDoan.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);

        layoutTabDonThuoc.setPadding(0, paddingTBTab, 0, paddingTBTab);
        imgTabDonThuoc.setLayoutParams(paramsImageTab);
        imgTabDonThuoc.setAdjustViewBounds(true);
        txtTabDonThuoc.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeTab);
    }

    private void addEvent() {

        txtLuuChanDoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkInput()){
                    Snackbar snackbar;
                    snackbar = Snackbar.make(v, "Vui lòng nhập đầy đủ thông tin!", Snackbar.LENGTH_SHORT);
                    View snackBarView = snackbar.getView();
                    snackBarView.setBackgroundColor(getResources().getColor(R.color.color_red));
                    TextView textView = snackBarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(getResources().getColor(R.color.color_white));
                    snackbar.show();
                }else {
                    createDiagnose();
                }
            }
        });

        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        layoutTabDoThi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PatientChartActivity.class));
                overridePendingTransition(0,0);
            }
        });

        layoutTabKetQua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PatientResultActivity.class));
                overridePendingTransition(0,0);
            }
        });

        layoutTabThongKe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PatientStatisticActivity.class));
                overridePendingTransition(0,0);
            }
        });

        layoutTabDonThuoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PatientPrescriptionActivity.class));
                overridePendingTransition(0,0);
            }
        });

    }

    private void createDiagnose() {
        ApiClient.getClient()
                .createDiagnosePatient(
                        0, Constant.PATIENT_BY_DOCTOR.getPID_ID(),
                        edtTenBenh.getText().toString().trim(), edtChanDoan.getText().toString().trim(),
                        edtHuongXuLy.getText().toString().trim(), edtXuLy.getText().toString().trim(),
                        null, null,
                        Constant.DOCTOR_LOGIN.getUserID(), edtLoiDan.getText().toString().trim(),
                        0, 0)
                .enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        Log.d("AAA", "CREATE NEW DIAGNOSE RESPONSE: " + response.body());
                        if (response.body() != null){
                            if (response.body().equals("success")){
                                Toast.makeText(PatientDiagnoseAddActivity.this, "Thêm chẩn đoán thành công", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Log.d("AAA", "CREATE NEW DIAGNOSE FAIL: " + t.getMessage());
                    }
                });
    }

    private boolean checkInput() {
        if (edtTenBenh.getText().toString().trim().isEmpty()){
            return false;
        }
        if (edtChanDoan.getText().toString().trim().isEmpty()){
            return false;
        }
        if (edtHuongXuLy.getText().toString().trim().isEmpty()){
            return false;
        }
        if (edtXuLy.getText().toString().trim().isEmpty()){
            return false;
        }
        if (edtLoiDan.getText().toString().trim().isEmpty()){
            return false;
        }
        return true;
    }
}
