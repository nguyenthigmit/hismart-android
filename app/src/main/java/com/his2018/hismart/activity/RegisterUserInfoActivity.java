package com.his2018.hismart.activity;

import android.app.DatePickerDialog;
import android.content.res.Configuration;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.his2018.hismart.R;
import com.his2018.hismart.adapter.AddressAdapter;
import com.his2018.hismart.helper.Helper;
import com.his2018.hismart.model.AddressPresponse;
import com.his2018.hismart.rest.ApiClient;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterUserInfoActivity extends AppCompatActivity {

    @BindView(R.id.layoutToolbar)   RelativeLayout layoutToolbar;
    @BindView(R.id.imgBackButton)   ImageView imgBackButton;
    @BindView(R.id.txtTitleToolbar) TextView txtTitleToolbar;
    @BindView(R.id.txtHISmart)      TextView txtHISmart;
    @BindView(R.id.txtViewer)       TextView txtViewer;
    @BindView(R.id.container_info)  ScrollView container_info;
    @BindView(R.id.layoutInfo)      LinearLayout layoutInfo;
    @BindView(R.id.txtMaSoBN)       TextView txtMaSoBN;
    @BindView(R.id.edtMaSo)         EditText edtMaSo;
    @BindView(R.id.txtHoTen)        TextView txtHoTen;
    @BindView(R.id.edtHoTen)        EditText edtHoTen;
    @BindView(R.id.txtNgaySinh)     TextView txtNgaySinh;
    @BindView(R.id.edtNgaySinh)     EditText edtNgaySinh;
    @BindView(R.id.layoutGioiTinh)  LinearLayout layoutGioiTinh;
    @BindView(R.id.txtGioiTinh)     TextView txtGioiTinh;
    @BindView(R.id.radioGroupGioiTinh)    RadioGroup radioGroupGioiTinh;
    @BindView(R.id.radioNam)        RadioButton radioNam;
    @BindView(R.id.radioNu)         RadioButton radioNu;
    @BindView(R.id.txtDiaChi)       TextView txtDiaChi;
    @BindView(R.id.edtDiaChi)       AutoCompleteTextView edtDiaChi;
    @BindView(R.id.txtSoDienThoai)  TextView txtSoDienThoai;
    @BindView(R.id.edtSoDienThoai)  EditText edtSoDienThoai;
    @BindView(R.id.txtEmail)        TextView txtEmail;
    @BindView(R.id.edtEmail)        EditText edtEmail;
    @BindView(R.id.btnTiepTheo)     Button btnTiepTheo;
    @BindView(R.id.container_password)      LinearLayout container_password;
    @BindView(R.id.txtMatKhau)              TextView txtMatKhau;
    @BindView(R.id.edtMatKhau)              EditText edtMatKhau;
    @BindView(R.id.txtNhapLaiMatKhau)       TextView txtNhapLaiMatKhau;
    @BindView(R.id.edtNhapLaiMatKhau)       EditText edtNhapLaiMatKhau;
    @BindView(R.id.btnHoanThanh)            Button btnHoanThanh;

    private String TAG = "REGISTER_UER_INFO";
    private int ID_ADDRESS = -1;
    private String BIRTH_DATE = "";
    private String SEX_ID = "0"; // Nam
    private boolean isLayoutInfo = true;
    Calendar calendar;
    SimpleDateFormat simpleDateFormat, dateFormatAPI;

    ArrayList<AddressPresponse> arrayAddress;
    AddressAdapter adapterAddress;

    int width, height;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_register_user_info);

        ButterKnife.bind(this);

        calendar = Calendar.getInstance();
        simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dateFormatAPI = new SimpleDateFormat("yyyy-dd-MM");

        addEvents();

        Configuration configuration = getResources().getConfiguration();
        if (configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
            setLayoutOritationPortrait();
        }else {
            setLayoutOrientationLandscape();
        }


        loadAddress();

    }

    private void loadAddress() {
        ApiClient.getClient().loadAddress()
                .enqueue(new Callback<List<AddressPresponse>>() {
                    @Override
                    public void onResponse(Call<List<AddressPresponse>> call, Response<List<AddressPresponse>> response) {
                        arrayAddress = new ArrayList<>();
                        for (AddressPresponse address : response.body()){
                            arrayAddress.add(address);
                            Log.d(TAG, address.toString());
                        }
                        // AutoCompleteTextView using adapter not work with adapterAddress.notifyDataSetChanged();
                        // set inner this function after load success
                        adapterAddress = new AddressAdapter(RegisterUserInfoActivity.this, R.layout.layout_row_auto_complete, arrayAddress);
                        edtDiaChi.setAdapter(adapterAddress);
                        edtDiaChi.setThreshold(1);
//                        Toast.makeText(RegisterUserInfoActivity.this, "Load Address Success", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<List<AddressPresponse>> call, Throwable t) {
                        Toast.makeText(RegisterUserInfoActivity.this, "Load Address Fail", Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "Load Address Fail: " + t.getMessage());
                    }
                });
    }

    private void setLayoutViewVertical() {
        /*
        * set layout info
        * */
        int padding10 = height/45;
        layoutToolbar.setPadding(padding10, padding10, padding10, padding10);
        int maxHeight30 = height/30;
        int paddingR15 = width/20;
        imgBackButton.setMaxHeight(maxHeight30);
        imgBackButton.setPadding(0, 0, paddingR15, 0);
        float text_size_dang_ki = height/25;
        txtTitleToolbar.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_dang_ki);
        float text_size_HISmart = height/25;
        txtHISmart.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_HISmart);
        float text_size_VIEWER = height/45;
        txtViewer.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_VIEWER);

        int padding15 = width/20;
        layoutInfo.setPadding(padding15, padding15, padding15, padding15);
        int paddingTop10 = height/85;
        txtMaSoBN.setPadding(0, paddingTop10, 0, 0);
        txtHoTen.setPadding(0, paddingTop10, 0, 0);
        txtNgaySinh.setPadding(0, paddingTop10, 0, 0);
        txtGioiTinh.setPadding(0, paddingTop10, 0, 0);
        radioGroupGioiTinh.setPadding(0, paddingTop10, 0, 0);
        int marginLeft20 = width/15;
        RadioGroup.LayoutParams paramsRadio = new RadioGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsRadio.setMargins(marginLeft20, 0, 0, 0);
        radioNu.setLayoutParams(paramsRadio);
        txtDiaChi.setPadding(0, paddingTop10, 0, 0);
        txtSoDienThoai.setPadding(0, paddingTop10, 0, 0);
        txtEmail.setPadding(0, paddingTop10, 0, 0);

        float text_size = height/35;
        txtMaSoBN.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        edtMaSo.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        txtHoTen.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        edtHoTen.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        txtNgaySinh.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        edtNgaySinh.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        txtGioiTinh.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        radioNam.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        radioNu.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        txtDiaChi.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        edtDiaChi.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        txtSoDienThoai.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        edtSoDienThoai.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        txtEmail.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        edtEmail.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);


        // set corner radius button
        GradientDrawable shape =  new GradientDrawable();
        shape.setCornerRadius(height/20);
        if(Build.VERSION.SDK_INT >= 23){
            shape.setColor(getResources().getColor(R.color.color_gray, null));
        }else {
            shape.setColor(getResources().getColor(R.color.color_gray));
        }
        btnTiepTheo.setBackground(shape);

        int marginTop50 = height/12;
        int paddingButton = height/50;
        int widthButton = width*2/3;
        LinearLayout.LayoutParams paramsBtnTiepTheo = new LinearLayout.LayoutParams(widthButton, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsBtnTiepTheo.setMargins(0, marginTop50, 0, 0);
        paramsBtnTiepTheo.gravity = Gravity.CENTER_HORIZONTAL;
        btnTiepTheo.setLayoutParams(paramsBtnTiepTheo);
        btnTiepTheo.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        btnTiepTheo.setPadding(0, paddingButton, 0, paddingButton);

        /*
        * set layout password
        * */
        int paddingLR15 = width/20;
        int paddingTop50 = height/12;
        container_password.setPadding(paddingLR15, paddingTop50, paddingLR15, 0);
        int paddingTop20 = height/35;
        txtMatKhau.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        edtMatKhau.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        txtNhapLaiMatKhau.setPadding(0, paddingTop20, 0, 0);
        txtNhapLaiMatKhau.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        edtNhapLaiMatKhau.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);

        // set corner radius button
        btnHoanThanh.setBackground(shape);

        LinearLayout.LayoutParams paramsBtnHoanThanh = new LinearLayout.LayoutParams(widthButton, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsBtnHoanThanh.setMargins(0, marginTop50, 0, 0);
        paramsBtnHoanThanh.gravity = Gravity.CENTER_HORIZONTAL;
        btnHoanThanh.setLayoutParams(paramsBtnHoanThanh);
        btnHoanThanh.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        btnHoanThanh.setPadding(0, paddingButton, 0, paddingButton);
    }

    private void setLayoutViewHorizontal() {
        /*
        * set layout info
        * */
        int padding10 = height/35;
        layoutToolbar.setPadding(padding10, padding10, padding10, padding10);
        int maxHeight30 = height/20;
        int paddingR15 = width/40;
        imgBackButton.setMaxHeight(maxHeight30);
        imgBackButton.setPadding(0, 0, paddingR15, 0);
        float text_size_dang_ki = height/25;
        txtTitleToolbar.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_dang_ki);
        float text_size_HISmart = height/25;
        txtHISmart.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_HISmart);
        float text_size_VIEWER = height/35;
        txtViewer.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_VIEWER);

        int padding15 = width/40;
        layoutInfo.setPadding(padding15, padding15, padding15, padding15);
        int paddingTop10 = height/45;
        txtMaSoBN.setPadding(0, paddingTop10, 0, 0);
        txtHoTen.setPadding(0, paddingTop10, 0, 0);
        txtNgaySinh.setPadding(0, paddingTop10, 0, 0);
        txtGioiTinh.setPadding(0, paddingTop10, 0, 0);
        radioGroupGioiTinh.setPadding(0, paddingTop10, 0, 0);
        int marginLeft20 = width/35;
        RadioGroup.LayoutParams paramsRadio = new RadioGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsRadio.setMargins(marginLeft20, 0, 0, 0);
        radioNu.setLayoutParams(paramsRadio);
        txtDiaChi.setPadding(0, paddingTop10, 0, 0);
        txtSoDienThoai.setPadding(0, paddingTop10, 0, 0);
        txtEmail.setPadding(0, paddingTop10, 0, 0);

        float text_size = height/25;
        txtMaSoBN.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        edtMaSo.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        txtHoTen.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        edtHoTen.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        txtNgaySinh.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        edtNgaySinh.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        txtGioiTinh.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        radioNam.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        radioNu.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        txtDiaChi.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        edtDiaChi.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        txtSoDienThoai.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        edtSoDienThoai.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        txtEmail.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        edtEmail.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);


        // set corner radius button
        GradientDrawable shape =  new GradientDrawable();
        shape.setCornerRadius(height/10);
        if(Build.VERSION.SDK_INT >= 23){
            shape.setColor(getResources().getColor(R.color.color_gray, null));
        }else {
            shape.setColor(getResources().getColor(R.color.color_gray));
        }
        btnTiepTheo.setBackground(shape);

        int marginTop50 = height/8;
        int paddingButton = height/30;
        int widthButton = width*2/3;
        LinearLayout.LayoutParams paramsBtnTiepTheo = new LinearLayout.LayoutParams(widthButton, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsBtnTiepTheo.setMargins(0, marginTop50, 0, 0);
        paramsBtnTiepTheo.gravity = Gravity.CENTER_HORIZONTAL;
        btnTiepTheo.setLayoutParams(paramsBtnTiepTheo);
        btnTiepTheo.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        btnTiepTheo.setPadding(0, paddingButton, 0, paddingButton);

        /*
        * set layout password
        * */
        int paddingLR15 = width/40;
        int paddingTop50 = height/7;
        container_password.setPadding(paddingLR15, paddingTop50, paddingLR15, 0);
        int paddingTop20 = height/25;
        txtMatKhau.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        edtMatKhau.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        txtNhapLaiMatKhau.setPadding(0, paddingTop20, 0, 0);
        txtNhapLaiMatKhau.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        edtNhapLaiMatKhau.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);

        // set corner radius button
        btnHoanThanh.setBackground(shape);

        LinearLayout.LayoutParams paramsBtnHoanThanh = new LinearLayout.LayoutParams(widthButton, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsBtnHoanThanh.setMargins(0, marginTop50, 0, 0);
        paramsBtnHoanThanh.gravity = Gravity.CENTER_HORIZONTAL;
        btnHoanThanh.setLayoutParams(paramsBtnHoanThanh);
        btnHoanThanh.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        btnHoanThanh.setPadding(0, paddingButton, 0, paddingButton);
    }

    private void addEvents() {

        imgBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLayoutCurrent();
            }
        });

        edtDiaChi.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AddressPresponse addressPresponse = (AddressPresponse) parent.getItemAtPosition(position);
                ID_ADDRESS = addressPresponse.getAddress_ID();
                checkInputInfo(); // case: last typing
//                Toast.makeText(RegisterUserInfoActivity.this, "ID ADDRESS: " + ID_ADDRESS, Toast.LENGTH_SHORT).show();
            }
        });

        btnTiepTheo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(getApplicationContext(), RegisterUserPasswordActivity.class));
                // hide layout info
                container_info.setVisibility(View.GONE);
                // show layout password
                container_password.setVisibility(View.VISIBLE);
                // set layout current
                isLayoutInfo = false;
            }
        });

        edtNgaySinh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(RegisterUserInfoActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        calendar.set(i, i1, i2);
                        edtNgaySinh.setText(simpleDateFormat.format(calendar.getTime()));
                        BIRTH_DATE = dateFormatAPI.format(calendar.getTime());
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
                datePickerDialog.show();
            };
        });


        edtMaSo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                checkInputInfo();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        edtHoTen.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                checkInputInfo();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        edtNgaySinh.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                checkInputInfo();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        radioGroupGioiTinh.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.radioNam: SEX_ID = "0";
                        break;
                    case R.id.radioNu: SEX_ID = "1";
                    break;
                }
            }
        });

        edtDiaChi.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 0){
                    ID_ADDRESS = -1;
                    Log.d(TAG, "ID ADDRESS: " + ID_ADDRESS);
                }
                checkInputInfo();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        edtSoDienThoai.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                checkInputInfo();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                checkInputInfo();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btnHoanThanh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(getApplicationContext(), RegisterUserCompleteActivity.class));
                String maBN         = edtMaSo.getText().toString().trim();
                String hoTen        = edtHoTen.getText().toString().trim();
                String soDienThoai  = edtSoDienThoai.getText().toString().trim();
                String email        = edtEmail.getText().toString().trim();
                String password     = edtMatKhau.getText().toString().trim();
                ApiClient.getClient().registerPatient(null, maBN, password, hoTen, BIRTH_DATE, ID_ADDRESS, 0, SEX_ID,
                        null, soDienThoai, email, 0, null, 0, null, null)
                        .enqueue(new Callback<String>() {
                            @Override
                            public void onResponse(Call<String> call, Response<String> response) {
                                Toast.makeText(RegisterUserInfoActivity.this, response.body(), Toast.LENGTH_SHORT).show();
                                Log.d(TAG, "REGISTER SUCCESS: " + response.body());
                            }

                            @Override
                            public void onFailure(Call<String> call, Throwable t) {
                                Toast.makeText(RegisterUserInfoActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                                Log.d(TAG, "REGISTER FAIL: " + t.getMessage());
                            }
                        });
            }
        });

        edtMatKhau.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                checkInputPassword();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        edtNhapLaiMatKhau.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                checkInputPassword();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void checkInputPassword(){
        String matKhau          = edtMatKhau.getText().toString().trim();
        String matKhauNhapLai   = edtNhapLaiMatKhau.getText().toString().trim();

        int colorGray, colorPink;
        if (Build.VERSION.SDK_INT >= 23){
            colorGray   = getResources().getColor(R.color.color_gray, null);
            colorPink   = getResources().getColor(R.color.color_pink_light, null);
        }else {
            colorGray   = getResources().getColor(R.color.color_gray);
            colorPink   = getResources().getColor(R.color.color_pink_light);
        }

        // set corner radius button
        GradientDrawable shape =  new GradientDrawable();
        shape.setCornerRadius(height/20);
        if ((matKhau.length() >= 4 && matKhauNhapLai.length() >= 4 ) && matKhau.equals(matKhauNhapLai)){
            btnHoanThanh.setEnabled(true);
            shape.setColor(colorPink);
        }else {
            btnHoanThanh.setEnabled(false);
            shape.setColor(colorGray);
        }
        btnHoanThanh.setBackground(shape);
    }

    /*
    * check layout showing is info or password
    * */
    private void checkLayoutCurrent() {
        if (isLayoutInfo){
            finish();
        }else {
            // hide layout password
            container_password.setVisibility(View.GONE);
            // show layout info
            container_info.setVisibility(View.VISIBLE);
            // set layout current
            isLayoutInfo = true;
        }
    }

    private void checkInputInfo(){
        String maSo     = edtMaSo.getText().toString().trim();
        String hoTen    = edtHoTen.getText().toString().trim();
        String ngaySinh = edtNgaySinh.getText().toString().trim();
        String soDT     = edtSoDienThoai.getText().toString().trim();
        String email    = edtEmail.getText().toString().trim();

        int colorGray, colorPink;
        if (Build.VERSION.SDK_INT >= 23){
            colorGray   = getResources().getColor(R.color.color_gray, null);
            colorPink   = getResources().getColor(R.color.color_pink_light, null);
        }else {
            colorGray   = getResources().getColor(R.color.color_gray);
            colorPink   = getResources().getColor(R.color.color_pink_light);
        }

        // set corner radius button
        GradientDrawable shape =  new GradientDrawable();
        shape.setCornerRadius(height/20);
        // ID_ADDRESS valid when != -1 (must choose from drop)
        if (maSo.length() == 0 || hoTen.length() == 0 || ngaySinh.length() == 0 || ID_ADDRESS == -1 || soDT.isEmpty() || isEmailValid() == false){
            btnTiepTheo.setEnabled(false);
            shape.setColor(colorGray);
        }else {
            btnTiepTheo.setEnabled(true);
            shape.setColor(colorPink);
        }
        btnTiepTheo.setBackground(shape);

    }

    private boolean isEmailValid(){
        String email = edtEmail.getText().toString().trim();
        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }

    @Override
    public void onBackPressed() {
        checkLayoutCurrent();
    }

    private void setLayoutOritationPortrait(){
        width   = Helper.getWidthScreen(this);
        height  = Helper.getHeightScreen(this);
        setLayoutViewVertical();
    }

    private void setLayoutOrientationLandscape(){
        width   = Helper.getWidthScreen(this);
        height  = Helper.getHeightScreen(this);
        setLayoutViewHorizontal();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setLayoutOrientationLandscape();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            setLayoutOritationPortrait();
        }
    }
}
