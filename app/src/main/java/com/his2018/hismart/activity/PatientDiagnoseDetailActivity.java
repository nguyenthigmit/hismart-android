package com.his2018.hismart.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.his2018.hismart.R;
import com.his2018.hismart.model.DiagnoseResponse;
import com.his2018.hismart.rest.ApiClient;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PatientDiagnoseDetailActivity extends AppCompatActivity {

    // toolbar
    @BindView(R.id.layoutToolbar)       LinearLayout layoutToolbar;
    @BindView(R.id.imgMainMenu)         ImageView imgMainMenu;
    @BindView(R.id.txtTitleToolbar)     TextView txtTitleToolbar;
    @BindView(R.id.imgPatientInfo)      ImageView imgPatientInfo;
    @BindView(R.id.imgPatientMessage)   ImageView imgPatientMessage;
    @BindView(R.id.imgPatientShare)     ImageView imgPatientShare;
    @BindView(R.id.layoutWarning)       RelativeLayout layoutWarning;
    @BindView(R.id.imgPatientWarning)   ImageView imgPatientWarning;
    @BindView(R.id.txtWarningCount)     TextView txtWarningCount;
    // tab bottom
    @BindView(R.id.layoutTabDoThi)      LinearLayout layoutTabDoThi;
    @BindView(R.id.imgTabDoThi)         ImageView imgTabDoThi;
    @BindView(R.id.txtTabDoThi)         TextView txtTabDoThi;
    @BindView(R.id.layoutTabKetQua)     LinearLayout layoutTabKetQua;
    @BindView(R.id.imgTabKetQua)        ImageView imgTabKetQua;
    @BindView(R.id.txtTabKetQua)        TextView txtTabKetQua;
    @BindView(R.id.layoutTabThongKe)    LinearLayout layoutTabThongKe;
    @BindView(R.id.imgTabThongKe)       ImageView imgTabThongKe;
    @BindView(R.id.txtTabThongKe)       TextView txtTabThongKe;
    @BindView(R.id.layoutTabChanDoan)   LinearLayout layoutTabChanDoan;
    @BindView(R.id.imgTabChanDoan)      ImageView imgTabChanDoan;
    @BindView(R.id.txtTabChanDoan)      TextView txtTabChanDoan;
    @BindView(R.id.layoutTabDonThuoc)   LinearLayout layoutTabDonThuoc;
    @BindView(R.id.imgTabDonThuoc)      ImageView imgTabDonThuoc;
    @BindView(R.id.txtTabDonThuoc)      TextView txtTabDonThuoc;

    @BindView(R.id.imgMenuDiagnose)     ImageView imgMenuDiagnose;
    @BindView(R.id.txtChanDoanDieuTri)  TextView txtChanDoanDieuTri;
    @BindView(R.id.txtCreateDate)       TextView txtCreateDate;
    @BindView(R.id.txtHuyXoa)           TextView txtHuyXoa;
    @BindView(R.id.layoutContentDiagnose1)  LinearLayout layoutContentDiagnose1;
    @BindView(R.id.txtBenhTitle)        TextView txtBenhTitle;
    @BindView(R.id.txtChanDoanTitle)    TextView txtChanDoanTitle;
    @BindView(R.id.txtBenh)             TextView txtBenh;
    @BindView(R.id.txtChanDoan)         TextView txtChanDoan;
    @BindView(R.id.layoutContentDiagnose2)  LinearLayout layoutContentDiagnose2;
    @BindView(R.id.txtHuongXuLyTitle)   TextView txtHuongXuLyTitle;
    @BindView(R.id.txtXuLyTitle)        TextView txtXuLyTitle;
    @BindView(R.id.txtLoiDanTitle)      TextView txtLoiDanTitle;
    @BindView(R.id.txtHuongXuLy)        TextView txtHuongXuLy;
    @BindView(R.id.txtXuLy)             TextView txtXuLy;
    @BindView(R.id.txtLoiDan)           TextView txtLoiDan;
    @BindView(R.id.fab)                 FloatingActionButton fab;

    SimpleDateFormat simpleDateFormat   = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    SimpleDateFormat dateFormat         = new SimpleDateFormat("dd/MM/yyyy");

    DiagnoseResponse diagnose =  null;
    Dialog dialogDelete = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_diagnose_detail);

        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT < 23){
            layoutTabChanDoan.setBackgroundColor(getResources().getColor(R.color.color_tab_selected));
        }else {
            layoutTabChanDoan.setBackgroundColor(getColor(R.color.color_tab_selected));
        }

        Intent intent = getIntent();
        if (intent.getExtras() != null){
            diagnose = (DiagnoseResponse) intent.getSerializableExtra("diagnoseData");
            if (diagnose != null){
                // convert string to Date
                try {
                    Date date = simpleDateFormat.parse(diagnose.getLastUpdate());
                    String lastUpdate = dateFormat.format(date);
                    txtCreateDate.setText(lastUpdate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                txtBenh.setText(diagnose.getMainSick());
                txtChanDoan.setText(diagnose.getDiagnose());
                txtHuongXuLy.setText(diagnose.getSolution());
                txtXuLy.setText(diagnose.getSolve());
                txtLoiDan.setText(diagnose.getAdvice());
            }
        }else {
            finish();
        }

        addEvent();
    }

    private void addEvent() {

        imgPatientInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), PatientInfomationActivity.class));
            }
        });

        imgMenuDiagnose.setOnClickListener(new View.OnClickListener() {

            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View view) {
                MenuBuilder menuBuilder = new MenuBuilder(PatientDiagnoseDetailActivity.this);
                MenuInflater inflater = new MenuInflater(PatientDiagnoseDetailActivity.this);
                inflater.inflate(R.menu.menu_diagnose_detail, menuBuilder);
                MenuPopupHelper optionsMenu = new MenuPopupHelper(PatientDiagnoseDetailActivity.this, menuBuilder, view);
                optionsMenu.setForceShowIcon(true);
                optionsMenu.show();

                menuBuilder.setCallback(new MenuBuilder.Callback() {
                    @Override
                    public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {

                        switch (item.getItemId()){
                            case R.id.menuXemDoThiHuyetAp:

                                break;
                            case R.id.menuXemLichSu:

                                break;
                            case R.id.menuChinhSua:
                                startActivity(new Intent(getApplicationContext(), PatientDiagnoseEditActivity.class));
                                break;
                            case R.id.menuXoaChanDoan:
                                showDialogDelete();
                                break;
                        }

                        return false;
                    }

                    @Override
                    public void onMenuModeChange(MenuBuilder menu) {

                    }
                });
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PatientDiagnoseAddActivity.class));
            }
        });

        layoutTabDoThi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PatientChartActivity.class));
                overridePendingTransition(0,0);
            }
        });

        layoutTabKetQua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PatientResultActivity.class));
                overridePendingTransition(0,0);
            }
        });

        layoutTabThongKe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PatientStatisticActivity.class));
                overridePendingTransition(0,0);
            }
        });

        layoutTabDonThuoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PatientPrescriptionActivity.class));
                overridePendingTransition(0,0);
            }
        });

    }

    private void showDialogDelete() {
        dialogDelete = new Dialog(PatientDiagnoseDetailActivity.this);
        dialogDelete.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogDelete.setContentView(R.layout.layout_dialog_delete);

        TextView txtQuestion    = dialogDelete.findViewById(R.id.txtQuestionConfirmDialog);
        Button btnConfirm       = dialogDelete.findViewById(R.id.btnConfirmDialogDelete);
        Button btnCancel        = dialogDelete.findViewById(R.id.btnCancelDialogDelete);

        String question  = getString(R.string.xoa_1_chan_doan);
        txtQuestion.setText(question);

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDiagnose(diagnose.getId());
                dialogDelete.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogDelete.dismiss();
            }
        });

        dialogDelete.show();
    }

    private void deleteDiagnose(int id) {

        ApiClient.getClient().deleteDiagnose(id)
                .enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        Log.d("AAA", "DELETE DIAGNOSE RESPONSE: " + response.body());
                        if (response != null){
                            if (response.body().equals("success")){
                                Toast.makeText(PatientDiagnoseDetailActivity.this, getString(R.string.xoa_thanh_cong), Toast.LENGTH_SHORT).show();
                                setResult(RESULT_OK);
                                finish();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Log.d("AAA", "DELETE DIAGNOSE FAIL: " + t.getMessage());
                    }
                });


    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }
}
