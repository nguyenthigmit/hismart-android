package com.his2018.hismart.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.his2018.hismart.R;
import com.his2018.hismart.helper.Helper;
import com.his2018.hismart.model.DoctorResponse;
import com.his2018.hismart.model.LoginResponse;
import com.his2018.hismart.model.Patient;
import com.his2018.hismart.rest.ApiClient;
import com.his2018.hismart.util.Constant;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {


    @BindView(R.id.layoutHISmart)       RelativeLayout layoutHISmart;
    @BindView(R.id.txtHISmart)          TextView txtHISmart;
    @BindView(R.id.txtViewer)           TextView txtViewer;
    @BindView(R.id.layoutLogin)         LinearLayout layoutLogin;
    @BindView(R.id.txtTitleMaSo)        TextView txtTitleMaSo;
    @BindView(R.id.edtMaSo)             EditText edtMaSo;
    @BindView(R.id.txtTitleMatKhau)     TextView txtTitleMatKhau;
    @BindView(R.id.edtMatKhau)          EditText edtMatKhau;
    @BindView(R.id.btnDangNhap)         Button btnDangNhap;
    @BindView(R.id.btnDangKi)           Button btnDangKi;
    @BindView(R.id.pbLoading)           ProgressBar pbLoading;

    int width, height;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        addEvents();

        Configuration configuration = getResources().getConfiguration();
        if (configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
            width   = Helper.getWidthScreen(this);
            height  = Helper.getHeightScreen(this);
            setLayoutViewHorizontal();
        }else {
            width   = Helper.getWidthScreen(this);
            height  = Helper.getHeightScreen(this);
            setLayoutViewVertical();
        }

    }

    private void checkInput() {
        String maSo     = edtMaSo.getText().toString().trim();
        String matKhau  = edtMatKhau.getText().toString().trim();
        int colorGray, colorBlue;
        if (Build.VERSION.SDK_INT >= 23){
            colorGray   = getResources().getColor(R.color.color_gray, null);
            colorBlue   = getResources().getColor(R.color.color_blue_app, null);
        }else {
            colorGray   = getResources().getColor(R.color.color_gray);
            colorBlue   = getResources().getColor(R.color.color_blue_app);
        }

        // set corner radius button
        GradientDrawable shape =  new GradientDrawable();
        shape.setCornerRadius(height/20);
        if (maSo.length() == 0 || matKhau.length() == 0){
            btnDangNhap.setEnabled(false);
            shape.setColor(colorGray);
        }else {
            btnDangNhap.setEnabled(true);
            shape.setColor(colorBlue);
        }
        btnDangNhap.setBackground(shape);
    }

    private void setLayoutViewHorizontal() {
        int paddingTB50 = height/10;
        layoutHISmart.setPadding(0, paddingTB50, 0, paddingTB50);
        txtHISmart.setTextSize(TypedValue.COMPLEX_UNIT_PX, height/25);
        txtViewer.setTextSize(TypedValue.COMPLEX_UNIT_PX, height/40);

        int paddingLR25 = width/10;
        layoutLogin.setPadding(paddingLR25, 0, paddingLR25, 0);
        float text_size = height/35;
        txtTitleMaSo.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        edtMaSo.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        txtTitleMatKhau.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        int paddingTop20 = height/20;
        txtTitleMatKhau.setPadding(0, paddingTop20, 0, 0);
        edtMatKhau.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);

        int marginTop40 = height/10;
        int paddingButton = height/50;
        LinearLayout.LayoutParams paramsBtnDangNhap = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsBtnDangNhap.setMargins(0, marginTop40, 0, 0);
        btnDangNhap.setLayoutParams(paramsBtnDangNhap);
        btnDangNhap.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        btnDangNhap.setPadding(0, paddingButton, 0, paddingButton);

        // set corner radius button
        GradientDrawable shape =  new GradientDrawable();
        shape.setCornerRadius(height/20);
        if(Build.VERSION.SDK_INT >= 23){
            shape.setColor(getResources().getColor(R.color.color_gray, null));
        }else {
            shape.setColor(getResources().getColor(R.color.color_gray));
        }
        btnDangNhap.setBackground(shape);

        int marginTop20 = height/20;
        LinearLayout.LayoutParams paramsBtnDangKi = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsBtnDangKi.setMargins(0, marginTop20, 0, 0);
        btnDangKi.setLayoutParams(paramsBtnDangKi);
        btnDangKi.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        btnDangKi.setPadding(0, paddingButton, 0, paddingButton);
    }

    private void setLayoutViewVertical() {
        int paddingTB50 = height/10;
        layoutHISmart.setPadding(0, paddingTB50, 0, paddingTB50);
        txtHISmart.setTextSize(TypedValue.COMPLEX_UNIT_PX, height/15);
        txtViewer.setTextSize(TypedValue.COMPLEX_UNIT_PX, height/20);

        int paddingLR25 = width/10;
        layoutLogin.setPadding(paddingLR25, 0, paddingLR25, 0);
        float text_size = height/20;
        txtTitleMaSo.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        edtMaSo.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        txtTitleMatKhau.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        int paddingTop20 = height/10;
        txtTitleMatKhau.setPadding(0, paddingTop20, 0, 0);
        edtMatKhau.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);

        int marginTop40 = height/5;
        int paddingButton = height/25;
        LinearLayout.LayoutParams paramsBtnDangNhap = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsBtnDangNhap.setMargins(0, marginTop40, 0, 0);
        btnDangNhap.setLayoutParams(paramsBtnDangNhap);
        btnDangNhap.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        btnDangNhap.setPadding(0, paddingButton, 0, paddingButton);

        // set corner radius button
        GradientDrawable shape =  new GradientDrawable();
        shape.setCornerRadius(height/5);
        if(Build.VERSION.SDK_INT >= 23){
            shape.setColor(getResources().getColor(R.color.color_gray, null));
        }else {
            shape.setColor(getResources().getColor(R.color.color_gray));
        }
        btnDangNhap.setBackground(shape);

        int marginTop20 = height/10;
        LinearLayout.LayoutParams paramsBtnDangKi = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsBtnDangKi.setMargins(0, marginTop20, 0, 0);
        btnDangKi.setLayoutParams(paramsBtnDangKi);
        btnDangKi.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        btnDangKi.setPadding(0, paddingButton, 0, paddingButton);
    }

    private void addEvents() {

        edtMaSo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                checkInput();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        edtMatKhau.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                checkInput();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btnDangKi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), RegisterUserInfoActivity.class));
            }
        });

        btnDangNhap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = edtMaSo.getText().toString().trim();
                String password = edtMatKhau.getText().toString().trim();
                loginUser(username, password);
            }
        });
    }

    private void loginUser(String username, String password){
        pbLoading.setVisibility(View.VISIBLE);
        ApiClient.getClient()
                .loginUser(username, password)
                .enqueue(new Callback<List<LoginResponse>>() {
                    @Override
                    public void onResponse(Call<List<LoginResponse>> call, Response<List<LoginResponse>> response) {
                        pbLoading.setVisibility(View.INVISIBLE);
                        Log.d("AAA", "RESPONSE: " +  response.body() );
                        if (response.body().size() > 0){
                            String fullName = response.body().get(0).getFullName();
                            if (response.body().get(0).getTypeUser().equals("doctor")){
                                Constant.IS_DOCTOR = true;
                                String id = response.body().get(0).getUserID();
                                loadDoctorLoginByUserID(id);
                                Toast.makeText(LoginActivity.this, "Xin chào bác sĩ: " + fullName, Toast.LENGTH_SHORT).show();
                            }else {
                                Constant.IS_DOCTOR = false;
                                String id = response.body().get(0).getUserID();
                                loadPatientLoginByID(id);
                                Toast.makeText(LoginActivity.this, "Xin chào bệnh nhân: " + fullName, Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            Toast.makeText(LoginActivity.this, R.string.dang_nhap_that_bat, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<LoginResponse>> call, Throwable t) {
                        pbLoading.setVisibility(View.INVISIBLE);
                        Log.d("AAA", "FAIL: " + t.getMessage());
                        Toast.makeText(LoginActivity.this, "Xảy ra lỗi, vui lòng thử lại!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void loadDoctorLoginByUserID(final String userID) {
        ApiClient.getClient().loadDoctorData()
                .enqueue(new Callback<List<DoctorResponse>>() {
                    @Override
                    public void onResponse(Call<List<DoctorResponse>> call, Response<List<DoctorResponse>> response) {
                        if (response.body().size() > 0){
                            for (DoctorResponse doctor : response.body()){
                                if (doctor.getUserID().equals(userID)){
                                    Constant.DOCTOR_LOGIN = doctor;
                                    startActivity(new Intent(getApplicationContext(), PatientListActivity.class));
                                    break;
                                }
                            }
                        }else {
                            Log.d("AAA", "LOAD DOCTOR DATA ERROR: " + response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<List<DoctorResponse>> call, Throwable t) {
                        Log.d("AAA", "LOAD DOCTOR DATA FAIL: " + t.getMessage());
                    }
                });
    }

    private void loadPatientLoginByID(final String id) {
        ApiClient.getClient().loadPatientsData()
                .enqueue(new Callback<List<Patient>>() {
                    @Override
                    public void onResponse(Call<List<Patient>> call, Response<List<Patient>> response) {
                        if (response.body().size() > 0){
                            Log.d("AAA", "RESPONSE COUNT: " + response.body().size());
                            for (Patient patient : response.body()){
                                Log.d("AAA", "PATIENT ID: " + id + " -> " + patient.getPID_ID());
                                if (patient.getPID_ID().equals(id)){
                                    Constant.PATIENT_LOGIN = patient;
                                    startActivity(new Intent(getApplicationContext(), PatientChartActivity.class));
                                    break;
                                }
                            }
                        }else {
                            Log.d("AAA", "LOAD PATIENT DATA ERROR: " + response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Patient>> call, Throwable t) {
                        Log.d("AAA", "LOAD PATIENT DATA FAIL: " + t.getMessage());
                    }
                });
    }
}
