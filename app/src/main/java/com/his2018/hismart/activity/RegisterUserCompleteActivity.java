package com.his2018.hismart.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.his2018.hismart.R;
import com.his2018.hismart.helper.Helper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterUserCompleteActivity extends AppCompatActivity {


    @BindView(R.id.layoutHISmart)       RelativeLayout layoutHISmart;
    @BindView(R.id.txtHISmart)          TextView txtHISmart;
    @BindView(R.id.txtViewer)           TextView txtViewer;
    @BindView(R.id.layoutContent)       LinearLayout layoutContent;
    @BindView(R.id.txtDangKiThanhCong)  TextView txtDangKiThanhCong;
    @BindView(R.id.txtVeTrangChu)       TextView txtVeTrangChu;
    @BindView(R.id.btnVeTrangChu)       Button btnVeTrangChu;

    int width, height;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_register_user_complete);

        ButterKnife.bind(this);

        Configuration configuration = getResources().getConfiguration();
        if (configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
            width   = Helper.getWidthScreen(this);
            height  = Helper.getHeightScreen(this);
            setLayoutViewVertical();

        }else {
            width   = Helper.getWidthScreen(this);
            height  = Helper.getHeightScreen(this);
            setLayoutViewHorizontal();
        }

        addEvents();
    }

    private void setLayoutViewVertical(){
        int paddingTop100 = height/6;
        layoutHISmart.setPadding(0, paddingTop100, 0, 0);
        float textSizeHISmart = height/20;
        txtHISmart.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeHISmart);
        float text_size = height/35;
        txtViewer.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        int paddingTop80 = height/8;
        layoutContent.setPadding(0, paddingTop80, 0, 0);
        txtDangKiThanhCong.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        int paddingTB30 = height/20;
        txtVeTrangChu.setPadding(0, paddingTB30, 0, paddingTB30);
        txtVeTrangChu.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);


        // set corner radius button
        GradientDrawable shape =  new GradientDrawable();
        shape.setCornerRadius(height/20);
        if(Build.VERSION.SDK_INT >= 23){
            shape.setColor(getResources().getColor(R.color.color_pink_light, null));
        }else {
            shape.setColor(getResources().getColor(R.color.color_pink_light));
        }
        btnVeTrangChu.setBackground(shape);

        int paddingButton = height/50;
        int widthButton = width*2/4;
        LinearLayout.LayoutParams paramsBtnHomeBack = new LinearLayout.LayoutParams(widthButton, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsBtnHomeBack.gravity = Gravity.CENTER_HORIZONTAL;
        btnVeTrangChu.setLayoutParams(paramsBtnHomeBack);
        btnVeTrangChu.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        btnVeTrangChu.setPadding(0, paddingButton, 0, paddingButton);
    }

    private void setLayoutViewHorizontal(){
        int paddingTop100 = height/8;
        layoutHISmart.setPadding(0, paddingTop100, 0, 0);
        float textSizeHISmart = height/15;
        txtHISmart.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeHISmart);
        float text_size = height/25;
        txtViewer.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        int paddingTop80 = height/8;
        layoutContent.setPadding(0, paddingTop80, 0, 0);
        txtDangKiThanhCong.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
        int paddingTB30 = height/10;
        txtVeTrangChu.setPadding(0, paddingTB30, 0, paddingTB30);
        txtVeTrangChu.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);


        // set corner radius button
        GradientDrawable shape =  new GradientDrawable();
        shape.setCornerRadius(height/10);
        if(Build.VERSION.SDK_INT >= 23){
            shape.setColor(getResources().getColor(R.color.color_pink_light, null));
        }else {
            shape.setColor(getResources().getColor(R.color.color_pink_light));
        }
        btnVeTrangChu.setBackground(shape);

        int paddingButton = height/20;
        int widthButton = width*2/5;
        LinearLayout.LayoutParams paramsBtnHomeBack = new LinearLayout.LayoutParams(widthButton, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsBtnHomeBack.gravity = Gravity.CENTER_HORIZONTAL;
        btnVeTrangChu.setLayoutParams(paramsBtnHomeBack);
        btnVeTrangChu.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
//        btnVeTrangChu.setPadding(0, paddingButton, 0, paddingButton);

    }

    private void addEvents() {
        btnVeTrangChu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PatientListActivity.class));
            }
        });
    }
}
