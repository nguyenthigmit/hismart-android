package com.his2018.hismart.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.his2018.hismart.R;
import com.his2018.hismart.model.PatientInfoResponse;
import com.his2018.hismart.rest.ApiClient;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BloodPressureWarningEditActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)             Toolbar toolbar;
    @BindView(R.id.txtTitleToolbar)     TextView txtTitleToolbar;
    @BindView(R.id.txtActionToolbar)    TextView txtActionToolbar;
    @BindView(R.id.layoutContent)       LinearLayout layoutContent;
    @BindView(R.id.txtThongSoTitle)     TextView txtThongSoTitle;
    @BindView(R.id.layoutThongSo)       LinearLayout layoutThongSo;
    @BindView(R.id.layoutLeft)          LinearLayout layoutLeft;
    @BindView(R.id.txtLowSystolicTitle) TextView txtLowSystolicTitle;
    @BindView(R.id.edtLowSystolic)      EditText edtLowSystolic;
    @BindView(R.id.txtPreHighSystolicTitle) TextView txtPreHighSystolicTitle;
    @BindView(R.id.edtPreHighSystolic)      EditText edtPreHighSystolic;
    @BindView(R.id.txtHighSystolicTitle)    TextView txtHighSystolicTitle;
    @BindView(R.id.edtHighSystolic)         EditText edtHighSystolic;
    @BindView(R.id.txtLowDiastolicTitle)    TextView txtLowDiastolicTitle;
    @BindView(R.id.edtLowDiastolic)         EditText edtLowDiastolic;
    @BindView(R.id.txtPreHighDiastolicTitle)    TextView txtPreHighDiastolicTitle;
    @BindView(R.id.edtPreHighDiastolic)         EditText edtPreHighDiastolic;
    @BindView(R.id.txtHighDiastolicTitle)       TextView txtHighDiastolicTitle;
    @BindView(R.id.edtHighDiastolic)            EditText edtHighDiastolic;

    PatientInfoResponse PATIENT_INFO = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blood_pressure_warning_edit);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        txtTitleToolbar.setText(com.his2018.hismart.R.string.chinh_sua_thong_so);
        txtActionToolbar.setVisibility(View.VISIBLE);
        txtActionToolbar.setText(R.string.luu_);

        Intent intent = getIntent();
        if (intent.getExtras() != null){
            PATIENT_INFO = (PatientInfoResponse) intent.getSerializableExtra("PATIENT_INFO");
            if (PATIENT_INFO != null){
                edtLowSystolic.setText(String.valueOf(PATIENT_INFO.getLowSystolic()));
                edtPreHighSystolic.setText(String.valueOf(PATIENT_INFO.getPreHighSystolic()));
                edtHighSystolic.setText(String.valueOf(PATIENT_INFO.getHighSystolic()));
                edtLowDiastolic.setText(String.valueOf(PATIENT_INFO.getLowDiastolic()));
                edtPreHighDiastolic.setText(String.valueOf(PATIENT_INFO.getPreHighDiastolic()));
                edtHighDiastolic.setText(String.valueOf(PATIENT_INFO.getHighDiastolic()));
            }
        }else {
            Toast.makeText(this, getString(R.string.xay_ra_loi), Toast.LENGTH_SHORT).show();
            finish();
        }

        addEvent();

    }

    private void addEvent() {
        txtActionToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkInput()){
                    update();
                }
            }
        });

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void update() {
        int lowSystolic         = Integer.parseInt(edtLowSystolic.getText().toString());
        int lowDiastolic        = Integer.parseInt(edtLowDiastolic.getText().toString());
        int preHighSystolic     = Integer.parseInt(edtPreHighSystolic.getText().toString());
        int preHighDiastolic    = Integer.parseInt(edtPreHighDiastolic.getText().toString());
        int highSystolic        = Integer.parseInt(edtHighSystolic.getText().toString());
        int highDiastolic       = Integer.parseInt(edtHighDiastolic.getText().toString());
        ApiClient.getClient()
                .updateHealthRecord(PATIENT_INFO.getPID_ID(),
                        lowSystolic, lowDiastolic,
                        preHighSystolic, preHighDiastolic,
                        highSystolic, highDiastolic)
                .enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        Log.d("AAA", "UPDATE HEALTH RECORD RESPONSE: " + response.body());
                        if (response.body().equals("success")){
                            Toast.makeText(BloodPressureWarningEditActivity.this, getString(R.string.cap_nhat_thanh_cong), Toast.LENGTH_SHORT).show();
//                            Intent intent = new Intent();
                            setResult(RESULT_OK);
//                            startActivity(intent);
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Log.d("AAA", "UPDATE HEALTH RECORD FAIL...");
                    }
                });
    }

    private boolean checkInput() {
        if (edtLowDiastolic.getText().toString().trim().isEmpty())
            return true;
        if (edtLowSystolic.getText().toString().trim().isEmpty())
            return true;
        if (edtPreHighDiastolic.getText().toString().trim().isEmpty())
            return true;
        if (edtPreHighSystolic.getText().toString().trim().isEmpty())
            return true;
        if (edtHighDiastolic.getText().toString().trim().isEmpty())
            return true;
        if (edtHighSystolic.getText().toString().trim().isEmpty())
            return true;

        return false;
    }


}
