package com.his2018.hismart.rest;

import com.his2018.hismart.model.AddressPresponse;
import com.his2018.hismart.model.BloodPressureResponse;
import com.his2018.hismart.model.DiagnoseResponse;
import com.his2018.hismart.model.DoctorResponse;
import com.his2018.hismart.model.LoginResponse;
import com.his2018.hismart.model.Patient;
import com.his2018.hismart.model.PatientByDoctor;
import com.his2018.hismart.model.PatientInfoResponse;

import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("Login")
    Call<List<LoginResponse>> loginUser(@Field("userName") String username,
                                          @Field("password") String password);

    @GET("Address")
    Call<List<AddressPresponse>> loadAddress();

    @GET("Users")
    Call<List<DoctorResponse>> loadDoctorData();

    @GET("Patients")
    Call<List<Patient>> loadPatientsData();

    @GET("GetPatientByUserID")
    Call<List<PatientByDoctor>> getPatientByDoctor(@Query("Id") String userID);

    @FormUrlEncoded
    @POST("Patients")
    Call<String> registerPatient(@Field("PID_ID") String pid_id, @Field("patient_ID") String pa_id,
                                 @Field("patient_Password") String password, @Field("patient_Name") String name,
                                 @Field("birth_Date") String birthDate, @Field("address_ID") int addr_id,
                                 @Field("idCard_Number") int idCard, @Field("sex_ID") String sex_id,
                                 @Field("occupation") String occupation, @Field("mobile_Phone") String phone,
                                 @Field("email") String email, @Field("status") int status,
                                 @Field("image") String image, @Field("remove") int remove,
                                 @Field("access_Lasttime") Date access_lasttime, @Field("access_Status") String access_status);

    @GET("BPOResult")
    Call<List<BloodPressureResponse>> getBloodPressurePatient(@Query("Id") String id);
    
    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "BPOResult", hasBody = true)
    Call<String> deleteBloodPressure(@Field("Id") int pip_id);

    @GET("Diagnosis")
    Call<List<DiagnoseResponse>> getDiagnosePatient(@Query("Id") String pip_id);

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "Diagnosis", hasBody = true)
    Call<String> deleteDiagnose(@Field("id") int id);

    /*
     * id: auto 0 / 1
     * CreateDate, LastUpdate, Status, Remove: not required
     * */
    @FormUrlEncoded
    @POST("Diagnosis")
    Call<String> createDiagnosePatient(@Field("Id") int id, @Field("PID_ID") String pip_id,
                                                       @Field("MainSick") String mainSick, @Field("Diagnose") String diagnose,
                                                       @Field("Solution") String solution, @Field("Solve") String solve,
                                                       @Field("CreateDate") Date createDate, @Field("LastUpdate") Date lastUpdate,
                                                       @Field("DoctorID") String doctorID, @Field("Advice") String advice,
                                                       @Field("Status") int status, @Field("Remove") int remove);

    @GET("GetBPOPatientByPIDID")
    Call<List<PatientInfoResponse>> getBloodPressureByPipId(@Query("Id") String pip_id);

    // cập nhật thông số huyết áp bệnh nhân
    @FormUrlEncoded
    @POST("HealthRecord")
    Call<String> updateHealthRecord(@Field("PID_ID") String pip_Id,
                                    @Field("LowSystolic") int lowSystolic, @Field("LowDiastolic") int lowDiastolic,
                                    @Field("PreHighSystolic") int preHighSystolic, @Field("PreHighDiastolic") int preHighDiastolic,
                                    @Field("HighSystolic") int highSystolic, @Field("HighDiastolic") int highDiastolic);

    @GET("GetListUserShareByPIDID")
    Call<List<DoctorResponse>> getDoctorShareByPatient(@Query("Id") String pip_id);

    @FormUrlEncoded
    @POST("Share")
    Call<String> sharePatientInfoByDoctor(@Field("PID_ID") String pip_Id, @Field("sUserID") String userID);


}
