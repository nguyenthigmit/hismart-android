package com.his2018.hismart.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.his2018.hismart.R;
import com.his2018.hismart.model.DoctorResponse;

import java.util.ArrayList;
import java.util.List;

public class DoctorShareAdapter extends ArrayAdapter<DoctorResponse> {

    private Context context;
    private int layout;
    private List<DoctorResponse> items, tempItems, suggestions;

    public DoctorShareAdapter(@NonNull Context context, int resource, @NonNull List<DoctorResponse> objects) {
        super(context, resource, objects);
        this.context = context;
        this.layout = resource;
        this.items = objects;
        tempItems = new ArrayList<>(items);
        suggestions = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public DoctorResponse getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder{
        TextView txtName;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if (convertView == null){
            convertView = LayoutInflater.from(context).inflate(layout, parent, false);
            holder = new ViewHolder();
            holder.txtName = convertView.findViewById(R.id.txtAutoCompleteRow);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }

        DoctorResponse doctor = getItem(position);

        holder.txtName.setText(doctor.toString());

        return convertView;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return doctorFilter;
    }

    private Filter doctorFilter = new Filter() {

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            DoctorResponse doctor = (DoctorResponse) resultValue;
            return doctor.toString();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null){
                suggestions.clear();
                for (DoctorResponse doctorPresponse : tempItems){
                    if (doctorPresponse.toString().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(doctorPresponse);
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return  filterResults;
            }else {
                return new FilterResults();
            }

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<DoctorResponse> doctor = (ArrayList<DoctorResponse>) results.values;
            if (results != null  && results.count > 0){
                clear();
                for (DoctorResponse dr : doctor){
                    add(dr);
                    notifyDataSetChanged();
                }
            }else {
                clear();
                notifyDataSetChanged();
            }
        }
    };
}
