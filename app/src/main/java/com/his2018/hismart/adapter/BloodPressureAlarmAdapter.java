package com.his2018.hismart.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.his2018.hismart.R;
import com.his2018.hismart.model.BloodPressureAlarm;

import java.text.SimpleDateFormat;
import java.util.List;

public class BloodPressureAlarmAdapter extends RecyclerView.Adapter<BloodPressureAlarmAdapter.ViewHolder>{

    private Activity activity;
    private List<BloodPressureAlarm> alarms;

    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

    public BloodPressureAlarmAdapter(Activity activity, List<BloodPressureAlarm> alarms) {
        this.activity = activity;
        this.alarms = alarms;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_row_blood_pressure_alarm, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        BloodPressureAlarm alarm = alarms.get(position);

        holder.txtName.setText(alarm.getFullName());
        holder.txtResult.setText(alarm.getResult());
        holder.txtDate.setText(dateFormat.format(alarm.getTime()));
        holder.txtTime.setText(timeFormat.format(alarm.getTime()));

        int COLOR_KIND = 0;
        if (alarm.getKind() == 1){
            COLOR_KIND = R.color.color_1;

        }else if (alarm.getKind() == 2){
            COLOR_KIND = activity.getResources().getColor(R.color.color_2);
        }else { // 3
            COLOR_KIND = activity.getResources().getColor(R.color.color_3);
        }
        holder.imgKind.setColorFilter(COLOR_KIND);
        holder.txtResult.setTextColor(COLOR_KIND);
    }

    @Override
    public int getItemCount() {
        return alarms.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private RelativeLayout layoutContain;
        private ImageView imgKind;
        private TextView txtName, txtResult, txtDate, txtTime;

        public ViewHolder(View itemView) {
            super(itemView);
            layoutContain   = itemView.findViewById(R.id.layoutContain);
            imgKind         = itemView.findViewById(R.id.imgAlarmKind);
            txtName         = itemView.findViewById(R.id.txtPatientName);
            txtResult       = itemView.findViewById(R.id.txtResult);
            txtDate         = itemView.findViewById(R.id.txtDate);
            txtTime         = itemView.findViewById(R.id.txtTime);
        }
    }
}
