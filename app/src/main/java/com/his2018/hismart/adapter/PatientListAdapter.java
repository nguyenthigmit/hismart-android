package com.his2018.hismart.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.his2018.hismart.R;
import com.his2018.hismart.activity.PatientChartActivity;
import com.his2018.hismart.helper.Helper;
import com.his2018.hismart.model.PatientByDoctor;
import com.his2018.hismart.util.Constant;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by TienMinh on 29-Jan-18.
 */

public class PatientListAdapter extends RecyclerView.Adapter<PatientListAdapter.ViewHolder>{

    private Activity activity;
    private List<PatientByDoctor> patients;
    private ArrayList<PatientByDoctor> itemsCopy;
    private int orientation;    // 1: vertical  - 2: horizontal

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    public PatientListAdapter(Activity activity, List<PatientByDoctor> patients, int orientation) {
        this.activity = activity;
        this.patients = patients;
        this.orientation = orientation;
        this.itemsCopy = new ArrayList<>(patients);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        if (orientation == 1){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_patient_vertical, parent, false);
        }else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_patient_horizontal, parent, false);
        }

        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        PatientByDoctor patient = patients.get(position);

        holder.txtSTT.setText(String.valueOf(position+1));
        holder.txtMaSoBN.setText(patient.getPID_ID());
        holder.txtHoTen.setText(patient.getPatient_Name());
        // convert string to Date
        String birthDay = "";
        try {
            Date date = simpleDateFormat.parse(patient.getBirth_Date());
            birthDay = dateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.txtNgaySinh.setText(birthDay);
        holder.txtSoDienThoai.setText(patient.getMobile_Phone());

        int alarmStatus = patient.getAlarmStatus();
        int STATUS_COLOR = 0;
        if (Build.VERSION.SDK_INT >= 23){
            if (alarmStatus == 0){
                STATUS_COLOR = activity.getResources().getColor(R.color.NormalWhiteColor, null);
            }else if(alarmStatus == 1){
                STATUS_COLOR = activity.getResources().getColor(R.color.AlarmBlueColor, null);
            }else if(alarmStatus == 2){
                STATUS_COLOR = activity.getResources().getColor(R.color.AlarmOrangeColor, null);
            }else if(alarmStatus == 3){
                STATUS_COLOR = activity.getResources().getColor(R.color.AlarmRedColor, null);
            }
        }else {
            if (alarmStatus == 0){
                STATUS_COLOR = activity.getResources().getColor(R.color.NormalWhiteColor);
            }else if(alarmStatus == 1){
                STATUS_COLOR = activity.getResources().getColor(R.color.AlarmBlueColor);
            }else if(alarmStatus == 2){
                STATUS_COLOR = activity.getResources().getColor(R.color.AlarmOrangeColor);
            }else if(alarmStatus == 3){
                STATUS_COLOR = activity.getResources().getColor(R.color.AlarmRedColor);
            }
        }
        holder.txtTrangThai.setTextColor(STATUS_COLOR);

        /*
        * set event click
        * */
        holder.layoutRowPatient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constant.PATIENT_BY_DOCTOR = patients.get(position);
                activity.startActivity(new Intent(activity, PatientChartActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return patients.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        private LinearLayout layoutRowPatient;
        private TextView txtSTT, txtMaSoBN, txtHoTen, txtNgaySinh, txtSoDienThoai, txtTrangThai;;

        int height  = Helper.getHeightScreen(activity);
        int width   = Helper.getWidthScreen(activity);

        public ViewHolder(final View itemView) {
            super(itemView);

            layoutRowPatient    = itemView.findViewById(R.id.layoutRowPatient);
            txtSTT              = itemView.findViewById(R.id.txtSTT_RowPatient);
            txtMaSoBN           = itemView.findViewById(R.id.txtCode_RowPatient);
            txtHoTen            = itemView.findViewById(R.id.txtHoTen_RowPatient);
            txtNgaySinh         = itemView.findViewById(R.id.txtNgaySinh_RowPatient);
            txtSoDienThoai      = itemView.findViewById(R.id.txtSoDienThoai_RowPatient);
            txtTrangThai        = itemView.findViewById(R.id.txtTrangThai_RowPatient);

            if (orientation == 1){ // VERTICAL
                int padding10 = height/70;
                layoutRowPatient.setPadding(padding10, padding10, padding10, padding10);
                float textSize = height/40;
                txtSTT.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
                txtMaSoBN.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
                txtHoTen.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
                txtNgaySinh.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
                txtSoDienThoai.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);

                float textSizeStatus = height/10;
                int marginTB_Minus30 = -height/20;
                LinearLayout.LayoutParams paramsStatus = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                paramsStatus.gravity = Gravity.RIGHT;
                paramsStatus.setMargins(0, marginTB_Minus30, 0, marginTB_Minus30);
                txtTrangThai.setLayoutParams(paramsStatus);
                txtTrangThai.setTypeface(Typeface.DEFAULT_BOLD);
                txtTrangThai.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeStatus);

            }else {
                int padding10 = height/45;
                layoutRowPatient.setPadding(padding10, padding10, padding10, padding10);
                float textSize = height/25;
                txtSTT.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
                txtMaSoBN.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
                txtHoTen.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
                txtNgaySinh.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
                txtSoDienThoai.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);

                float textSizeStatus = height/10;
                int marginTB_Minus30 = -height/20;
                LinearLayout.LayoutParams paramsStatus = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                paramsStatus.gravity = Gravity.RIGHT;
                paramsStatus.setMargins(0, marginTB_Minus30, 0, marginTB_Minus30);
                txtTrangThai.setLayoutParams(paramsStatus);
                txtTrangThai.setTypeface(Typeface.DEFAULT_BOLD);
                txtTrangThai.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeStatus);
            }
        }
    }

    public void filter(String text) {
        patients.clear();
        if(text.length() == 0){
            patients.addAll(itemsCopy);
        } else{
            text = text.toLowerCase();
            for(PatientByDoctor item: itemsCopy){
                if(item.getPatient_Name().toLowerCase().contains(text)){
                    patients.add(item);
                }
            }
        }
        notifyDataSetChanged();
    }
}
