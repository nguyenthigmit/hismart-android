package com.his2018.hismart.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.his2018.hismart.R;
import com.his2018.hismart.model.AddressPresponse;

import java.util.ArrayList;
import java.util.List;

public class AddressAdapter extends ArrayAdapter<AddressPresponse> {

    private Context context;
    private int layout;
    private List<AddressPresponse> items, tempItems, suggestions;

    public AddressAdapter(@NonNull Context context, int resource, @NonNull List<AddressPresponse> objects) {
        super(context, resource, objects);
        this.context = context;
        this.layout = resource;
        this.items = objects;
        tempItems = new ArrayList<>(items);
        suggestions = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public AddressPresponse getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder{
        TextView txtName;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if (convertView == null){
            convertView = LayoutInflater.from(context).inflate(layout, parent, false);
            holder = new ViewHolder();
            holder.txtName = convertView.findViewById(R.id.txtAutoCompleteRow);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }

        AddressPresponse address = getItem(position);

        holder.txtName.setText(address.toString());

        return convertView;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return addressFilter;
    }

    private Filter addressFilter = new Filter() {

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            AddressPresponse address = (AddressPresponse) resultValue;
            return address.toString();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null){
                suggestions.clear();
                for (AddressPresponse addressPresponse : tempItems){
                    if (addressPresponse.toString().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(addressPresponse);
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return  filterResults;
            }else {
                return new FilterResults();
            }

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<AddressPresponse> address = (ArrayList<AddressPresponse>) results.values;
            if (results != null  && results.count > 0){
                clear();
                for (AddressPresponse addr : address){
                    add(addr);
                    notifyDataSetChanged();
                }
            }else {
                clear();
                notifyDataSetChanged();
            }
        }
    };
}
