package com.his2018.hismart.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.his2018.hismart.R;
import com.his2018.hismart.model.PatientInfoShared;

import java.text.SimpleDateFormat;
import java.util.List;

public class PatientInfoSharedAdapter extends RecyclerView.Adapter<PatientInfoSharedAdapter.ViewHolder> {

    private Activity activity;
    private List<PatientInfoShared> sharedList;

    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    public PatientInfoSharedAdapter(Activity activity, List<PatientInfoShared> sharedList) {
        this.activity = activity;
        this.sharedList = sharedList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_row_patient_shared, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        PatientInfoShared infoShared = sharedList.get(position);

        holder.txtPatientName.setText(infoShared.getPatientName());
        holder.txtDoctorName.setText(infoShared.getDoctorName());
        holder.txtTime.setText(dateFormat.format(infoShared.getTime()));
        /*
        * Trạng thái huyết áo của bệnh nhân: 1 bình thường, 2 thấp, 3 cao
        * */
        int COLOR_STATUS = 0;
        if (infoShared.getStatus() == 1){
            COLOR_STATUS = activity.getResources().getColor(R.color.color_1);
        }else if(infoShared.getStatus() == 2){
            COLOR_STATUS = activity.getResources().getColor(R.color.color_2);
        }else if(infoShared.getStatus() == 3){
            COLOR_STATUS = activity.getResources().getColor(R.color.color_3);
        }
        holder.imgStatus.setColorFilter(COLOR_STATUS);

        if (infoShared.getNote().equals("")){
            holder.imgNote.setVisibility(View.INVISIBLE);
        }else {
            holder.imgNote.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return sharedList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView txtPatientName, txtDoctorName, txtTime;
        private ImageView imgStatus, imgNote, imgShareOff;

        public ViewHolder(View itemView) {
            super(itemView);

            txtPatientName  = itemView.findViewById(R.id.txtPatientName);
            txtDoctorName   = itemView.findViewById(R.id.txtDoctorName);
            txtTime         = itemView.findViewById(R.id.txtDateShare);
            imgStatus       = itemView.findViewById(R.id.imgStatus);
            imgNote         = itemView.findViewById(R.id.imgShareNote);
            imgShareOff     = itemView.findViewById(R.id.imgShareOff);
        }
    }
}
