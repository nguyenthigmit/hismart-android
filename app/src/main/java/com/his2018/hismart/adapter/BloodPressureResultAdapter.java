package com.his2018.hismart.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.his2018.hismart.R;
import com.his2018.hismart.helper.Helper;
import com.his2018.hismart.model.BloodPressureResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by TienMinh on 29-Mar-18.
 */

public class BloodPressureResultAdapter extends RecyclerView.Adapter<BloodPressureResultAdapter.ViewHolder>{

    private Activity activity;
    private List<BloodPressureResponse> bloodPressures;
    private int orientation;    // 1: vertical  - 2: horizontal

    SimpleDateFormat simpleDateFormat   = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    SimpleDateFormat dateFormat         = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat timeFormat         = new SimpleDateFormat("HH:mm:ss");

    public BloodPressureResultAdapter(Activity activity, List<BloodPressureResponse> bloodPressures, int orientation) {
        this.activity = activity;
        this.bloodPressures = bloodPressures;
        this.orientation = orientation;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_row_blood_pressure, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final BloodPressureResponse bloodPressure = bloodPressures.get(position);

        // trạng thái huyết áp có 4 loại tương ứng 4 màu:
        // 0 - NormalWhiteColor
        // 1 - AlarmBlueColor
        // 2 - AlarmOrangeColor
        // 3 - AlarmRedColor

//        int STATUS_COLOR = 0;
//        if (Build.VERSION.SDK_INT >= 23){
//            if (bloodPressure.getKind() == 1){
//                STATUS_COLOR = activity.getResources().getColor(R.color.color_gray, null);
//            }else if(bloodPressure.getKind() == 2){
//                STATUS_COLOR = activity.getResources().getColor(R.color.color_blue_light, null);
//            }else {
//                STATUS_COLOR = activity.getResources().getColor(R.color.color_red, null);
//            }
//        }else {
//            if (bloodPressure.getKind() == 1){
//                STATUS_COLOR = activity.getResources().getColor(R.color.color_gray);
//            }else if(bloodPressure.getKind() == 2){
//                STATUS_COLOR = activity.getResources().getColor(R.color.color_blue_light);
//            }else{
//                STATUS_COLOR = activity.getResources().getColor(R.color.color_red);
//            }
//        }
//        holder.imgStatus.setColorFilter(STATUS_COLOR);

        // convert string to Date
        String observationDate = "", observationTime = "";
        try {
            Date date = simpleDateFormat.parse(bloodPressure.getObservation_Date());
            observationDate = dateFormat.format(date);
            observationTime = timeFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.txtDate.setText(observationDate);
        holder.txtTime.setText(observationTime);
        // gán màu cho 3 thông số: đỏ / xanh dương /  xanh lá

        String text = "<font color='red'>"+ bloodPressure.getSYST() +
                "</font> / <font color='blue'>"+ bloodPressure.getDIAS() +
                "</font> / <font color='green'>"+ bloodPressure.getPULS() + "</font>";
        holder.txtResult.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);

        if (bloodPressure.isShow()){
            holder.cbRemove.setVisibility(View.VISIBLE);
        }else {
            holder.cbRemove.setVisibility(View.INVISIBLE);
        }

        holder.cbRemove.setChecked(bloodPressures.get(position).isChecked());
        holder.cbRemove.setTag(bloodPressures.get(position));

        holder.cbRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckBox cb = (CheckBox) view;
                BloodPressureResponse model = (BloodPressureResponse) cb.getTag();

                model.setChecked(cb.isChecked());
                bloodPressures.get(position).setChecked(cb.isChecked());
            }
        });

    }

    @Override
    public int getItemCount() {
        return bloodPressures.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private RelativeLayout layoutContain;
        private ImageView imgStatus;
        private TextView txtDate, txtTime, txtResult;
        private CheckBox cbRemove;

        int height  = Helper.getHeightScreen(activity);
        int width   = Helper.getWidthScreen(activity);

        public ViewHolder(View itemView) {
            super(itemView);
            layoutContain   = itemView.findViewById(R.id.layout_contain);
            imgStatus       = itemView.findViewById(R.id.imgStatus);
            txtDate         = itemView.findViewById(R.id.txtDate);
            txtTime         = itemView.findViewById(R.id.txtTime);
            txtResult       = itemView.findViewById(R.id.txtResults);
            cbRemove        = itemView.findViewById(R.id.cbRemove);

            if (orientation == 1){ // VERTICAL
                int paddingTLB10 = height/50;
                int paddingR20 = width/40;
                layoutContain.setPadding(paddingTLB10, paddingTLB10, paddingR20, paddingTLB10);

                int imgWidth15 = height/45;
                int marginR40 = width/15;
                RelativeLayout.LayoutParams paramImgStatus = new RelativeLayout.LayoutParams(imgWidth15, ViewGroup.LayoutParams.WRAP_CONTENT);
                paramImgStatus.setMargins(0, 0, marginR40, 0);
                imgStatus.setLayoutParams(paramImgStatus);
                imgStatus.setAdjustViewBounds(true);

                float text_size = height/40;
                float text_size_time = height/50;
                int marginR20 = width/30;
                RelativeLayout.LayoutParams paramTextDate = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                paramTextDate.setMargins(0, 0, marginR20, 0);
                paramTextDate.addRule(RelativeLayout.RIGHT_OF, imgStatus.getId());
                paramTextDate.addRule(RelativeLayout.ALIGN_TOP, imgStatus.getId());
                txtDate.setLayoutParams(paramTextDate);
                txtDate.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);

                txtResult.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
                txtTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_time);

            }else {
                int paddingTLB10 = height/30;
                int paddingR20 = width/70;
                layoutContain.setPadding(paddingTLB10, paddingTLB10, paddingR20, paddingTLB10);

                int imgWidth15 = height/30;
                int marginR40 = width/25;
                RelativeLayout.LayoutParams paramImgStatus = new RelativeLayout.LayoutParams(imgWidth15, ViewGroup.LayoutParams.WRAP_CONTENT);
                paramImgStatus.setMargins(0, 0, marginR40, 0);
                imgStatus.setLayoutParams(paramImgStatus);
                imgStatus.setAdjustViewBounds(true);

                float text_size = height/25;
                float text_size_time = height/30;
                int marginR20 = width/30;
                RelativeLayout.LayoutParams paramTextDate = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                paramTextDate.setMargins(0, 0, marginR20, 0);
                paramTextDate.addRule(RelativeLayout.RIGHT_OF, imgStatus.getId());
                paramTextDate.addRule(RelativeLayout.ALIGN_TOP, imgStatus.getId());
                txtDate.setLayoutParams(paramTextDate);
                txtDate.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);

                txtResult.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
                txtTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size_time);
            }
        }
    }
}
