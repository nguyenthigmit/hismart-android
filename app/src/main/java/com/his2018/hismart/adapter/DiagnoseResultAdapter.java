package com.his2018.hismart.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.his2018.hismart.R;
import com.his2018.hismart.activity.PatientDiagnoseActivity;
import com.his2018.hismart.activity.PatientDiagnoseDetailActivity;
import com.his2018.hismart.helper.Helper;
import com.his2018.hismart.model.DiagnoseResponse;
import com.his2018.hismart.model.DoctorResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by TienMinh on 29-Mar-18.
 */

public class DiagnoseResultAdapter extends RecyclerView.Adapter<DiagnoseResultAdapter.ViewHolder>{

    private Activity activity;
    private List<DiagnoseResponse> diagnoses;
    private int orientation;    // 1: vertical  - 2: horizontal
    private List<DoctorResponse> doctors;

    SimpleDateFormat simpleDateFormat   = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    SimpleDateFormat dateFormat         = new SimpleDateFormat("dd/MM/yyyy");

    public DiagnoseResultAdapter(Activity activity, List<DiagnoseResponse> Diagnoses, int orientation, List<DoctorResponse> doctors) {
        this.activity = activity;
        this.diagnoses = Diagnoses;
        this.orientation = orientation;
        this.doctors = doctors;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_row_diagnose, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final DiagnoseResponse diagnose = diagnoses.get(position);

        if (diagnose != null){
            // convert string to Date
            try {
                Date date = simpleDateFormat.parse(diagnose.getLastUpdate());
                String createDate = dateFormat.format(date);
                holder.txtDate.setText(createDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            for (DoctorResponse doctor : doctors){
                if (doctor.getUserID().equals(diagnose.getDoctorID())){
                    holder.txtDoctor.setText(doctor.getFullName());
                    break;
                }
            }
        }

        if (diagnose.isShow()){
            holder.cbRemove.setVisibility(View.VISIBLE);
        }else {
            holder.cbRemove.setVisibility(View.INVISIBLE);
        }

        holder.cbRemove.setChecked(diagnoses.get(position).isChecked());
        holder.cbRemove.setTag(diagnoses.get(position));

        holder.cbRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckBox cb = (CheckBox) view;
                DiagnoseResponse model = (DiagnoseResponse) cb.getTag();

                model.setChecked(cb.isChecked());
                diagnoses.get(position).setChecked(cb.isChecked());
            }
        });

        holder.layoutContain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, PatientDiagnoseDetailActivity.class);
                intent.putExtra("diagnoseData", diagnose);
                activity.startActivityForResult(intent, ((PatientDiagnoseActivity) activity).REQUEST_DETAIL);
            }
        });

    }

    @Override
    public int getItemCount() {
        return diagnoses.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private RelativeLayout layoutContain;
        private TextView txtDate, txtDoctor;
        private CheckBox cbRemove;

        int height  = Helper.getHeightScreen(activity);
        int width   = Helper.getWidthScreen(activity);

        public ViewHolder(final View itemView) {
            super(itemView);
            layoutContain   = itemView.findViewById(R.id.layout_contain);
            txtDate         = itemView.findViewById(R.id.txtDateDiagnose);
            txtDoctor       = itemView.findViewById(R.id.txtDoctorDiagnose);
            cbRemove        = itemView.findViewById(R.id.cbRemove);

            if (orientation == 1){ // VERTICAL
                int paddingTLB10 = height/50;
                layoutContain.setPadding(paddingTLB10, paddingTLB10, paddingTLB10, paddingTLB10);

                float text_size = height/40;
                txtDate.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
                txtDoctor.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);

            }else {
                int paddingTLB10 = height/30;
                layoutContain.setPadding(paddingTLB10, paddingTLB10, paddingTLB10, paddingTLB10);

                float text_size = height/25;
                txtDate.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
                txtDoctor.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
            }

        }
    }
}
