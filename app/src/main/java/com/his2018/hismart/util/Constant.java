package com.his2018.hismart.util;

import com.his2018.hismart.model.DoctorResponse;
import com.his2018.hismart.model.Patient;
import com.his2018.hismart.model.PatientByDoctor;

public class Constant {
    public static boolean IS_DOCTOR = true;
    public static DoctorResponse DOCTOR_LOGIN = null;
    public static PatientByDoctor PATIENT_BY_DOCTOR = null;
    public static Patient PATIENT_LOGIN = null;
}
