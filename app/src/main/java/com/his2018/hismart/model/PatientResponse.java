package com.his2018.hismart.model;

public class PatientResponse {
    private String UserID;
    private String UserName;
    private String FullName;
    private int Gender;
    private String Doctor_Degree;
    private String Mobile_Phone;
    private String DoctorAssistant_Enable;
    private int DepartmentId;
    private long RankId;
    private int Status;
    private String MedicalCenterID;

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public int getGender() {
        return Gender;
    }

    public void setGender(int gender) {
        Gender = gender;
    }

    public String getDoctor_Degree() {
        return Doctor_Degree;
    }

    public void setDoctor_Degree(String doctor_Degree) {
        Doctor_Degree = doctor_Degree;
    }

    public String getMobile_Phone() {
        return Mobile_Phone;
    }

    public void setMobile_Phone(String mobile_Phone) {
        Mobile_Phone = mobile_Phone;
    }

    public String getDoctorAssistant_Enable() {
        return DoctorAssistant_Enable;
    }

    public void setDoctorAssistant_Enable(String doctorAssistant_Enable) {
        DoctorAssistant_Enable = doctorAssistant_Enable;
    }

    public int getDepartmentId() {
        return DepartmentId;
    }

    public void setDepartmentId(int departmentId) {
        DepartmentId = departmentId;
    }

    public long getRankId() {
        return RankId;
    }

    public void setRankId(long rankId) {
        RankId = rankId;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getMedicalCenterID() {
        return MedicalCenterID;
    }

    public void setMedicalCenterID(String medicalCenterID) {
        MedicalCenterID = medicalCenterID;
    }

    @Override
    public String toString() {
        return FullName + " " + UserID + " " + UserName;
    }
}
