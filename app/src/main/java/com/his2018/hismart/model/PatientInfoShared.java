package com.his2018.hismart.model;

import java.util.Date;

public class PatientInfoShared {
    private String PatientName;
    private String DoctorName;
    private String Note;
    private int Status;
    private Date Time;

    public PatientInfoShared(String patientName, String doctorName, String note, int status, Date time) {
        PatientName = patientName;
        DoctorName = doctorName;
        Note = note;
        Status = status;
        Time = time;
    }

    public String getPatientName() {
        return PatientName;
    }

    public void setPatientName(String patientName) {
        PatientName = patientName;
    }

    public String getDoctorName() {
        return DoctorName;
    }

    public void setDoctorName(String doctorName) {
        DoctorName = doctorName;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String note) {
        Note = note;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public Date getTime() {
        return Time;
    }

    public void setTime(Date time) {
        Time = time;
    }
}
