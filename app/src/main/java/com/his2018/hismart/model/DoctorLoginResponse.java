package com.his2018.hismart.model;

public class DoctorLoginResponse {
    private String UserID;
    private String UserName;
    private String FullName;
    private int Gender;
    private String MedicalCenterID;
    private int DepartmentId;
    private String Mobile_Phone;
    private String Doctor_Degree;
    private long RankId;
    private int Status;
    private String DoctorAssistant_Enable;
    private String Access_LastTime;
    private String Acc_Status;
    private String TypeUser;

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public int getGender() {
        return Gender;
    }

    public void setGender(int gender) {
        Gender = gender;
    }

    public String getMedicalCenterID() {
        return MedicalCenterID;
    }

    public void setMedicalCenterID(String medicalCenterID) {
        MedicalCenterID = medicalCenterID;
    }

    public int getDepartmentId() {
        return DepartmentId;
    }

    public void setDepartmentId(int departmentId) {
        DepartmentId = departmentId;
    }

    public String getMobile_Phone() {
        return Mobile_Phone;
    }

    public void setMobile_Phone(String mobile_Phone) {
        Mobile_Phone = mobile_Phone;
    }

    public String getDoctor_Degree() {
        return Doctor_Degree;
    }

    public void setDoctor_Degree(String doctor_Degree) {
        Doctor_Degree = doctor_Degree;
    }

    public long getRankId() {
        return RankId;
    }

    public void setRankId(long rankId) {
        RankId = rankId;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getDoctorAssistant_Enable() {
        return DoctorAssistant_Enable;
    }

    public void setDoctorAssistant_Enable(String doctorAssistant_Enable) {
        DoctorAssistant_Enable = doctorAssistant_Enable;
    }

    public String getAccess_LastTime() {
        return Access_LastTime;
    }

    public void setAccess_LastTime(String access_LastTime) {
        Access_LastTime = access_LastTime;
    }

    public String getAcc_Status() {
        return Acc_Status;
    }

    public void setAcc_Status(String acc_Status) {
        Acc_Status = acc_Status;
    }

    public String getTypeUser() {
        return TypeUser;
    }

    public void setTypeUser(String typeUser) {
        TypeUser = typeUser;
    }
}
