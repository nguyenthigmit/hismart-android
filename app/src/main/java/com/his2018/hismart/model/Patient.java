package com.his2018.hismart.model;

/**
 * Created by TienMinh on 29-Jan-18.
 */

public class Patient {
    private String PID_ID;
    private String Patient_ID;
    private String Patient_Password;
    private String Patient_Name;
    private String Birth_Date;
    private int Address_ID;
    private long IdCard_Number;
    private String Sex_ID;
    private String Occupation;
    private String Mobile_Phone;
    private String Email;
    private int Status;
    private String Image;
    private String Remove;
    private String Access_LastTime;
    private String Acc_Status;
    private int AlarmStatus;

    public String getPID_ID() {
        return PID_ID;
    }

    public void setPID_ID(String PID_ID) {
        this.PID_ID = PID_ID;
    }

    public String getPatient_ID() {
        return Patient_ID;
    }

    public void setPatient_ID(String patient_ID) {
        Patient_ID = patient_ID;
    }

    public String getPatient_Password() {
        return Patient_Password;
    }

    public void setPatient_Password(String patient_Password) {
        Patient_Password = patient_Password;
    }

    public String getPatient_Name() {
        return Patient_Name;
    }

    public void setPatient_Name(String patient_Name) {
        Patient_Name = patient_Name;
    }

    public String getBirth_Date() {
        return Birth_Date;
    }

    public void setBirth_Date(String birth_Date) {
        Birth_Date = birth_Date;
    }

    public int getAddress_ID() {
        return Address_ID;
    }

    public void setAddress_ID(int address_ID) {
        Address_ID = address_ID;
    }

    public long getIdCard_Number() {
        return IdCard_Number;
    }

    public void setIdCard_Number(long idCard_Number) {
        IdCard_Number = idCard_Number;
    }

    public String getSex_ID() {
        return Sex_ID;
    }

    public void setSex_ID(String sex_ID) {
        Sex_ID = sex_ID;
    }

    public String getOccupation() {
        return Occupation;
    }

    public void setOccupation(String occupation) {
        Occupation = occupation;
    }

    public String getMobile_Phone() {
        return Mobile_Phone;
    }

    public void setMobile_Phone(String mobile_Phone) {
        Mobile_Phone = mobile_Phone;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getRemove() {
        return Remove;
    }

    public void setRemove(String remove) {
        Remove = remove;
    }

    public String getAccess_LastTime() {
        return Access_LastTime;
    }

    public void setAccess_LastTime(String access_LastTime) {
        Access_LastTime = access_LastTime;
    }

    public String getAcc_Status() {
        return Acc_Status;
    }

    public void setAcc_Status(String acc_Status) {
        Acc_Status = acc_Status;
    }

    public int getAlarmStatus() {
        return AlarmStatus;
    }

    public void setAlarmStatus(int alarmStatus) {
        AlarmStatus = alarmStatus;
    }
}
