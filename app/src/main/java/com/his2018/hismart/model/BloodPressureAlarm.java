package com.his2018.hismart.model;

import java.util.Date;

/**
 * Created by TienMinh on 29-Mar-18.
 */

public class BloodPressureAlarm {
    private int Id;
    private String FullName;
    private int Kind;
    private String Result;
    private Date Time;

    public BloodPressureAlarm(int id, String fullName, int kind, String result, Date time) {
        Id = id;
        FullName = fullName;
        Kind = kind;
        Result = result;
        Time = time;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public int getKind() {
        return Kind;
    }

    public void setKind(int kind) {
        Kind = kind;
    }

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    public Date getTime() {
        return Time;
    }

    public void setTime(Date time) {
        Time = time;
    }
}
