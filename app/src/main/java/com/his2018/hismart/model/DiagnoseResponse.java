package com.his2018.hismart.model;

import java.io.Serializable;

/**
 * Created by TienMinh on 09-Apr-18.
 */
// Chẩn đoán
public class DiagnoseResponse implements Serializable{
    private int Id;
    private String PID_ID;
    private String MainSick;
    private String Diagnose;
    private String Solution;
    private String Solve;
    private String CreateDate;
    private String LastUpdate;
    private String DoctorID;
    private String Advice;
    private int Status;
    private int Remove;
    private boolean isShow;
    private boolean isChecked;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getPID_ID() {
        return PID_ID;
    }

    public void setPID_ID(String PID_ID) {
        this.PID_ID = PID_ID;
    }

    public String getMainSick() {
        return MainSick;
    }

    public void setMainSick(String mainSick) {
        MainSick = mainSick;
    }

    public String getDiagnose() {
        return Diagnose;
    }

    public void setDiagnose(String diagnose) {
        Diagnose = diagnose;
    }

    public String getSolution() {
        return Solution;
    }

    public void setSolution(String solution) {
        Solution = solution;
    }

    public String getSolve() {
        return Solve;
    }

    public void setSolve(String solve) {
        Solve = solve;
    }

    public String getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(String createDate) {
        CreateDate = createDate;
    }

    public String getLastUpdate() {
        return LastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        LastUpdate = lastUpdate;
    }

    public String getDoctorID() {
        return DoctorID;
    }

    public void setDoctorID(String doctorID) {
        DoctorID = doctorID;
    }

    public String getAdvice() {
        return Advice;
    }

    public void setAdvice(String advice) {
        Advice = advice;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public int getRemove() {
        return Remove;
    }

    public void setRemove(int remove) {
        Remove = remove;
    }

    public boolean isShow() {
        return isShow;
    }

    public void setShow(boolean show) {
        isShow = show;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
