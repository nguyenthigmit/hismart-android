package com.his2018.hismart.model;

public class AddressPresponse {
    private int Address_ID;
    private String Street;
    private String Other_Destination;
    private String City;

    @Override
    public String toString() {
        return Street + ", " + Other_Destination + ", " + City;
    }

    public int getAddress_ID() {
        return Address_ID;
    }

    public void setAddress_ID(int address_ID) {
        Address_ID = address_ID;
    }

    public String getStreet() {
        return Street;
    }

    public void setStreet(String street) {
        Street = street;
    }

    public String getOther_Destination() {
        return Other_Destination;
    }

    public void setOther_Destination(String other_Destination) {
        Other_Destination = other_Destination;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }
}
