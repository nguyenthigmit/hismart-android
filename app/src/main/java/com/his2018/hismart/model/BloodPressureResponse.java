package com.his2018.hismart.model;

/**
 * Created by TienMinh on 29-Mar-18.
 */

public class BloodPressureResponse {
    private int BPOBR_ID;
    private String PID_ID;
    private String Observation_Date;
    private int SYST;
    private int DIAS;
    private int PULS;
    private int Remove;
    private boolean isShow; // use to show checkbox
    private boolean isChecked; // use to selected

    public int getBPOBR_ID() {
        return BPOBR_ID;
    }

    public void setBPOBR_ID(int BPOBR_ID) {
        this.BPOBR_ID = BPOBR_ID;
    }

    public String getPID_ID() {
        return PID_ID;
    }

    public void setPID_ID(String PID_ID) {
        this.PID_ID = PID_ID;
    }

    public String getObservation_Date() {
        return Observation_Date;
    }

    public void setObservation_Date(String observation_Date) {
        Observation_Date = observation_Date;
    }

    public int getSYST() {
        return SYST;
    }

    public void setSYST(int SYST) {
        this.SYST = SYST;
    }

    public int getDIAS() {
        return DIAS;
    }

    public void setDIAS(int DIAS) {
        this.DIAS = DIAS;
    }

    public int getPULS() {
        return PULS;
    }

    public void setPULS(int PULS) {
        this.PULS = PULS;
    }

    public int getRemove() {
        return Remove;
    }

    public void setRemove(int remove) {
        Remove = remove;
    }

    public boolean isShow() {
        return isShow;
    }

    public void setShow(boolean show) {
        isShow = show;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
