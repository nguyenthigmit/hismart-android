package com.his2018.hismart.model;

import java.io.Serializable;

public class PatientInfoResponse implements Serializable{
    private int HR_ID;
    private String PID_ID;
    private String DoctorID;
    private String BloodType;
    private int Height;
    private int Weight;
    private String Disease;
    private String Medical_Condition;
    private String Appointment_Date;
    private String Appointment_Confirm;
    private String TBPM_ID;
    private String TECG_ID;
    private String TSPIR_ID;
    private String Service_Expire_Date;
    private String Access_LastTime;
    private int LowSystolic;
    private int LowDiastolic;
    private int PreHighSystolic;
    private int PreHighDiastolic;
    private int HighSystolic;
    private int HighDiastolic;
    private int AlarmStatus;

    public int getHR_ID() {
        return HR_ID;
    }

    public String getPID_ID() {
        return PID_ID;
    }

    public String getDoctorID() {
        return DoctorID;
    }

    public String getBloodType() {
        return BloodType;
    }

    public int getHeight() {
        return Height;
    }

    public int getWeight() {
        return Weight;
    }

    public String getDisease() {
        return Disease;
    }

    public String getMedical_Condition() {
        return Medical_Condition;
    }

    public String getAppointment_Date() {
        return Appointment_Date;
    }

    public String getAppointment_Confirm() {
        return Appointment_Confirm;
    }

    public String getTBPM_ID() {
        return TBPM_ID;
    }

    public String getTECG_ID() {
        return TECG_ID;
    }

    public String getTSPIR_ID() {
        return TSPIR_ID;
    }

    public String getService_Expire_Date() {
        return Service_Expire_Date;
    }

    public String getAccess_LastTime() {
        return Access_LastTime;
    }

    public int getLowSystolic() {
        return LowSystolic;
    }

    public int getLowDiastolic() {
        return LowDiastolic;
    }

    public int getPreHighSystolic() {
        return PreHighSystolic;
    }

    public int getPreHighDiastolic() {
        return PreHighDiastolic;
    }

    public int getHighSystolic() {
        return HighSystolic;
    }

    public int getHighDiastolic() {
        return HighDiastolic;
    }

    public int getAlarmStatus() {
        return AlarmStatus;
    }
}
